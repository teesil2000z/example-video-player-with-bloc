import 'comments_bloc/failure_test.dart';
import 'comments_bloc/success_test.dart';
import 'flow_bloc/failure_test.dart';
import 'flow_bloc/success_test.dart';
import 'navigation_bloc/success_test.dart';
import 'user_bloc/failure_test.dart';
import 'user_bloc/success_test.dart';
import 'video_bloc/failure_test.dart';
import 'video_bloc/success_test.dart';

void main() async{

  //comments bloc test
  await commentsFailure();
  await commentsSuccess();

  //flow bloc tests
  await flowFailure();
  await flowSuccess();

  //navigation bloc test
  await navigationSuccess();

  //user bloc tests
  await userFailure();
  await userSuccess();

  //video bloc tests
  await videoFailure();
  await videoSuccess();

}