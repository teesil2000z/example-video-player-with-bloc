import 'dart:convert';

import 'package:bloc_example/data/video/datasourse/video_remote_data_source.dart';
import 'package:bloc_example/data/video/repositories/video_repository.dart';
import 'package:bloc_example/data/video/usecases/get_related_videos.dart';
import 'package:bloc_example/data/video/usecases/get_video.dart';
import 'package:bloc_example/di/video/video_bloc.dart';
import 'package:bloc_example/di/video/video_event.dart';
import 'package:bloc_example/di/video/video_state.dart';
import 'package:bloc_example/model/global_auth_state.dart';
import 'package:bloc_example/model/token_model.dart';
import 'package:bloc_example/model/video_model.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http_mock_adapter/http_mock_adapter.dart';

void main() {
  group('Video Bloc Success Scenarios: ', () {
    late Dio dio;
    GlobalAuthState globalAuthState = GlobalAuthState();
    globalAuthState.token = const TokenModel(
        token:
            "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IjEyMzEyMzEyMyIsImVtYWlsIjoiampAZ21haWwuY29tIiwiZXhwIjoxNjc0ODgyNjYxfQ.JvxSIlZ1-xQV9I0XXN4WDY5zh6EF3T6Z6ehgGcQjw8E",
        id: 1,
        name: 'test user',
        email: 'test@gmail.com');
    VideoRemoteDataSource videoRemoteDataSource =
        VideoRemoteDataSource(globalAuthState: globalAuthState);
    VideoRepository videoRepository =
        VideoRepository(remoteDataSource: videoRemoteDataSource);
    GetRelatedVideos allVideos =
        GetRelatedVideos(videoRepository: videoRepository);
    GetVideo getVideo = GetVideo(videoRepository: videoRepository);
    late DioAdapter dioAdapter;
    setUp(() {
      dio = Dio();
      dioAdapter = DioAdapter(dio: dio);
      dio.httpClientAdapter = dioAdapter;
    });

    blocTest<VideoBloc, VideoState>(
      'When data is empty',
      act: (bloc) => bloc.add(VideoLoadEvent(id: 1)),
      build: () => VideoBloc(allVideos: allVideos, getVideo: getVideo),
      wait: const Duration(milliseconds: 5000),
      expect: () => [
        VideoLoadingState(),
        VideoLoadedState(
            model: const VideoModel(
                id: 1,
                name: 'name',
                path: 'path',
                description: 'description',
                userId: 1),
            data: const [])
      ],
    );

    String data =
        '[{"id":1,"name":"Sun","description":"Sun storm","path":"https://firebasestorage.googleapis.com/v0/b/test-task-67bb2.appspot.com/o/wild_4.mp4?alt=media\u0026token=8cd085ec-5f46-4b4a-8ee7-255c8e14d679","user_id":3},{"id":2,"name":"Forest","description":"Forest 1","path":"https://firebasestorage.googleapis.com/v0/b/test-task-67bb2.appspot.com/o/wild_2.mp4?alt=media\u0026token=1a708a0d-950b-4bd3-b1ae-6213fc1142ad","user_id":3},{"id":4,"name":"Forest","description":"Forest 3","path":"https://firebasestorage.googleapis.com/v0/b/test-task-67bb2.appspot.com/o/timer_1.mp4?alt=media\u0026token=d4a7aed5-bacf-43a6-9162-9bfd29908b81","user_id":3}]';
    setUp(() {
      dio = Dio();
      dioAdapter = DioAdapter(dio: dio);
      dio.httpClientAdapter = dioAdapter;
    });
    blocTest<VideoBloc, VideoState>(
      'When data is not empty',
      act: (bloc) => bloc.add(VideoLoadEvent(id: 1)),
      build: () => VideoBloc(allVideos: allVideos, getVideo: getVideo),
      wait: const Duration(milliseconds: 5000),
      expect: () => [
        VideoLoadingState(),
        VideoLoadedState(
            model: const VideoModel(
                id: 3,
                name: 'name',
                path: 'path',
                description: 'description',
                userId: 3),
            data: (json.decode(data) as List)
                .map((data) => VideoModel.fromJson(data))
                .toList())
      ],
    );
  });
}

Future<void> videoSuccess() async{
  group('Video Bloc Success Scenarios: ', () {
    late Dio dio;
    GlobalAuthState globalAuthState = GlobalAuthState();
    globalAuthState.token = const TokenModel(
        token:
            "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IjEyMzEyMzEyMyIsImVtYWlsIjoiampAZ21haWwuY29tIiwiZXhwIjoxNjc0ODgyNjYxfQ.JvxSIlZ1-xQV9I0XXN4WDY5zh6EF3T6Z6ehgGcQjw8E",
        id: 1,
        name: 'test user',
        email: 'test@gmail.com');
    VideoRemoteDataSource videoRemoteDataSource =
        VideoRemoteDataSource(globalAuthState: globalAuthState);
    VideoRepository videoRepository =
        VideoRepository(remoteDataSource: videoRemoteDataSource);
    GetRelatedVideos allVideos =
        GetRelatedVideos(videoRepository: videoRepository);
    GetVideo getVideo = GetVideo(videoRepository: videoRepository);
    late DioAdapter dioAdapter;
    setUp(() {
      dio = Dio();
      dioAdapter = DioAdapter(dio: dio);
      dio.httpClientAdapter = dioAdapter;
    });

    blocTest<VideoBloc, VideoState>(
      'When data is empty',
      act: (bloc) => bloc.add(VideoLoadEvent(id: 1)),
      build: () => VideoBloc(allVideos: allVideos, getVideo: getVideo),
      wait: const Duration(milliseconds: 5000),
      expect: () => [
        VideoLoadingState(),
        VideoLoadedState(
            model: const VideoModel(
                id: 1,
                name: 'name',
                path: 'path',
                description: 'description',
                userId: 1),
            data: const [])
      ],
    );

    String data =
        '[{"id":1,"name":"Sun","description":"Sun storm","path":"https://firebasestorage.googleapis.com/v0/b/test-task-67bb2.appspot.com/o/wild_4.mp4?alt=media\u0026token=8cd085ec-5f46-4b4a-8ee7-255c8e14d679","user_id":3},{"id":2,"name":"Forest","description":"Forest 1","path":"https://firebasestorage.googleapis.com/v0/b/test-task-67bb2.appspot.com/o/wild_2.mp4?alt=media\u0026token=1a708a0d-950b-4bd3-b1ae-6213fc1142ad","user_id":3},{"id":4,"name":"Forest","description":"Forest 3","path":"https://firebasestorage.googleapis.com/v0/b/test-task-67bb2.appspot.com/o/timer_1.mp4?alt=media\u0026token=d4a7aed5-bacf-43a6-9162-9bfd29908b81","user_id":3}]';
    setUp(() {
      dio = Dio();
      dioAdapter = DioAdapter(dio: dio);
      dio.httpClientAdapter = dioAdapter;
    });
    blocTest<VideoBloc, VideoState>(
      'When data is not empty',
      act: (bloc) => bloc.add(VideoLoadEvent(id: 1)),
      build: () => VideoBloc(allVideos: allVideos, getVideo: getVideo),
      wait: const Duration(milliseconds: 5000),
      expect: () => [
        VideoLoadingState(),
        VideoLoadedState(
            model: const VideoModel(
                id: 3,
                name: 'name',
                path: 'path',
                description: 'description',
                userId: 3),
            data: (json.decode(data) as List)
                .map((data) => VideoModel.fromJson(data))
                .toList())
      ],
    );
  });
}
