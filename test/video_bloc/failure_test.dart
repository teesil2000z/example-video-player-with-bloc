import 'package:bloc_example/data/video/datasourse/video_remote_data_source.dart';
import 'package:bloc_example/data/video/repositories/video_repository.dart';
import 'package:bloc_example/data/video/usecases/get_related_videos.dart';
import 'package:bloc_example/data/video/usecases/get_video.dart';
import 'package:bloc_example/di/video/video_bloc.dart';
import 'package:bloc_example/di/video/video_event.dart';
import 'package:bloc_example/di/video/video_state.dart';
import 'package:bloc_example/model/global_auth_state.dart';
import 'package:bloc_example/model/token_model.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http_mock_adapter/http_mock_adapter.dart';
void main(){
  group('Video Bloc Error scenarios: ', () {
    late Dio dio;
    late DioAdapter dioAdapter;
    GlobalAuthState globalAuthState = GlobalAuthState();
    globalAuthState.token = const TokenModel(
      token: '',
      id: 1, 
      name: 'test', 
      email: 'test@gmail.com'
    );
    VideoRemoteDataSource videoRemoteDataSource = VideoRemoteDataSource(globalAuthState: globalAuthState);
    VideoRepository videoRepository = VideoRepository(remoteDataSource: videoRemoteDataSource);
    GetRelatedVideos allVideos = GetRelatedVideos(videoRepository: videoRepository);
    GetVideo getVideo = GetVideo(videoRepository: videoRepository);

    setUp(() {
      dio = Dio();
      dioAdapter = DioAdapter(dio: dio);
      dio.httpClientAdapter = dioAdapter;
    });

    blocTest<VideoBloc, VideoState>(
      'emits failure at initial response is null',
      act: (bloc)=>bloc.add(VideoLoadEvent(id: 3)),
      build: () => VideoBloc(allVideos: allVideos, getVideo: getVideo),
      wait: const Duration(milliseconds: 5000),
      expect: () =>
          [VideoLoadingState(), VideoErrorState()],
    );
  });
}

Future<void> videoFailure() async {
  group('Video Bloc Error scenarios: ', () {
    late Dio dio;
    late DioAdapter dioAdapter;
    GlobalAuthState globalAuthState = GlobalAuthState();
    globalAuthState.token = const TokenModel(
      token: '',
      id: 1, 
      name: 'test', 
      email: 'test@gmail.com'
    );
    VideoRemoteDataSource videoRemoteDataSource = VideoRemoteDataSource(globalAuthState: globalAuthState);
    VideoRepository videoRepository = VideoRepository(remoteDataSource: videoRemoteDataSource);
    GetRelatedVideos allVideos = GetRelatedVideos(videoRepository: videoRepository);
    GetVideo getVideo = GetVideo(videoRepository: videoRepository);

    setUp(() {
      dio = Dio();
      dioAdapter = DioAdapter(dio: dio);
      dio.httpClientAdapter = dioAdapter;
    });

    blocTest<VideoBloc, VideoState>(
      'emits failure at initial response is null',
      act: (bloc)=>bloc.add(VideoLoadEvent(id: 1)),
      build: () => VideoBloc(allVideos: allVideos, getVideo: getVideo),
      wait: const Duration(milliseconds: 5000),
      expect: () =>
          [VideoLoadingState(), VideoErrorState()],
    );
  });
}