import 'package:bloc_example/data/video/datasourse/video_remote_data_source.dart';
import 'package:bloc_example/data/video/repositories/video_repository.dart';
import 'package:bloc_example/data/video/usecases/create_comment.dart';
import 'package:bloc_example/data/video/usecases/delete_comment.dart';
import 'package:bloc_example/data/video/usecases/get_comments.dart';
import 'package:bloc_example/data/video/usecases/update_comment.dart';
import 'package:bloc_example/di/comments/comments_cubit.dart';
import 'package:bloc_example/model/global_auth_state.dart';
import 'package:bloc_example/model/token_model.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http_mock_adapter/http_mock_adapter.dart';
void main(){
  group('Comments Bloc Error scenarios: ', () {
    late Dio dio;
    late DioAdapter dioAdapter;
    GlobalAuthState globalAuthState = GlobalAuthState();
    globalAuthState.token = const TokenModel(
      token: '',
      id: 1, 
      name: 'test', 
      email: 'test@gmail.com'
    );
    VideoRemoteDataSource videoRemoteDataSource = VideoRemoteDataSource(globalAuthState: globalAuthState);
    VideoRepository videoRepository = VideoRepository(remoteDataSource: videoRemoteDataSource);
    CreateComment createComment = CreateComment(videoRepository: videoRepository);
    DeleteComment deleteComment = DeleteComment(videoRepository: videoRepository);
    GetComment getComment = GetComment(videoRepository: videoRepository);
    UpdateComment updateComment = UpdateComment(videoRepository: videoRepository);


    setUp(() {
      dio = Dio();
      dioAdapter = DioAdapter(dio: dio);
      dio.httpClientAdapter = dioAdapter;
    });

    blocTest<CommentsCubit, CommentsState>(
      'emits failure',
      act: (bloc)=>bloc.loadComments(0),
      build: () => CommentsCubit(createComment: createComment, getComment: getComment, deleteComment: deleteComment, updateComment: updateComment),
      wait: const Duration(milliseconds: 5000),
      expect: () =>
          [const CommentsLoadingState(comment: ''), const CommentsErrorState(comment: '')],
    );
  });
}

Future<void> commentsFailure() async{
  group('Comments Bloc Error scenarios: ', () {
    late Dio dio;
    late DioAdapter dioAdapter;
    GlobalAuthState globalAuthState = GlobalAuthState();
    globalAuthState.token = const TokenModel(
      token: '',
      id: 1, 
      name: 'test', 
      email: 'test@gmail.com'
    );
    VideoRemoteDataSource videoRemoteDataSource = VideoRemoteDataSource(globalAuthState: globalAuthState);
    VideoRepository videoRepository = VideoRepository(remoteDataSource: videoRemoteDataSource);
    CreateComment createComment = CreateComment(videoRepository: videoRepository);
    DeleteComment deleteComment = DeleteComment(videoRepository: videoRepository);
    GetComment getComment = GetComment(videoRepository: videoRepository);
    UpdateComment updateComment = UpdateComment(videoRepository: videoRepository);


    setUp(() {
      dio = Dio();
      dioAdapter = DioAdapter(dio: dio);
      dio.httpClientAdapter = dioAdapter;
    });

    blocTest<CommentsCubit, CommentsState>(
      'emits failure',
      act: (bloc)=>bloc.loadComments(0),
      build: () => CommentsCubit(createComment: createComment, getComment: getComment, deleteComment: deleteComment, updateComment: updateComment),
      wait: const Duration(milliseconds: 5000),
      expect: () =>
          [const CommentsLoadingState(comment: ''), const CommentsErrorState(comment: '')],
    );
  });
}