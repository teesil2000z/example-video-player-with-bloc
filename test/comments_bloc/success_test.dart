import 'dart:convert';

import 'package:bloc_example/data/video/datasourse/video_remote_data_source.dart';
import 'package:bloc_example/data/video/repositories/video_repository.dart';
import 'package:bloc_example/data/video/usecases/create_comment.dart';
import 'package:bloc_example/data/video/usecases/delete_comment.dart';
import 'package:bloc_example/data/video/usecases/get_comments.dart';
import 'package:bloc_example/data/video/usecases/update_comment.dart';
import 'package:bloc_example/di/comments/comments_cubit.dart';
import 'package:bloc_example/model/comment_model.dart';
import 'package:bloc_example/model/global_auth_state.dart';
import 'package:bloc_example/model/token_model.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http_mock_adapter/http_mock_adapter.dart';

void main() {
  group('Video Bloc Success scenarios: ', () {
    late Dio dio;
    late DioAdapter dioAdapter;
    GlobalAuthState globalAuthState = GlobalAuthState();
    globalAuthState.token = const TokenModel(
        token: 
            "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IjEyMzEyMzEyMyIsImVtYWlsIjoiampAZ21haWwuY29tIiwiZXhwIjoxNjc0ODgyNjYxfQ.JvxSIlZ1-xQV9I0XXN4WDY5zh6EF3T6Z6ehgGcQjw8E",
        id: 1, name: 'test', email: 'test@gmail.com');
    VideoRemoteDataSource videoRemoteDataSource =
        VideoRemoteDataSource(globalAuthState: globalAuthState);
    VideoRepository videoRepository =
        VideoRepository(remoteDataSource: videoRemoteDataSource);
    CreateComment createComment =
        CreateComment(videoRepository: videoRepository);
    DeleteComment deleteComment =
        DeleteComment(videoRepository: videoRepository);
    GetComment getComment = GetComment(videoRepository: videoRepository);
    UpdateComment updateComment =
        UpdateComment(videoRepository: videoRepository);
    String mockData =
        '[{"id":43,"comment":"asd","name":"John Johnes","video_id":6,"user_id":4}]';

    setUp(() {
      dio = Dio();
      dioAdapter = DioAdapter(dio: dio);
      dio.httpClientAdapter = dioAdapter;
    });

    blocTest<CommentsCubit, CommentsState>(
      'emits success',
      act: (bloc) => bloc.loadComments(6),
      build: () => CommentsCubit(
          createComment: createComment,
          getComment: getComment,
          deleteComment: deleteComment,
          updateComment: updateComment),
      wait: const Duration(milliseconds: 5000),
      expect: () => [
        const CommentsLoadingState(comment: ''),
        CommentAddedState(comment: '',
            comments: (json.decode(mockData) as List).map((model) {
          CommentModel data = CommentModel.fromJson(model);
          return data;
        }).toList())
      ],
    );
  });
}

Future<void> commentsSuccess() async{
  group('Video Bloc Success scenarios: ', () {
    late Dio dio;
    late DioAdapter dioAdapter;
    GlobalAuthState globalAuthState = GlobalAuthState();
    globalAuthState.token = const TokenModel(
        token: 
            "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IjEyMzEyMzEyMyIsImVtYWlsIjoiampAZ21haWwuY29tIiwiZXhwIjoxNjc0ODgyNjYxfQ.JvxSIlZ1-xQV9I0XXN4WDY5zh6EF3T6Z6ehgGcQjw8E",
        id: 1, name: 'test', email: 'test@gmail.com');
    VideoRemoteDataSource videoRemoteDataSource =
        VideoRemoteDataSource(globalAuthState: globalAuthState);
    VideoRepository videoRepository =
        VideoRepository(remoteDataSource: videoRemoteDataSource);
    CreateComment createComment =
        CreateComment(videoRepository: videoRepository);
    DeleteComment deleteComment =
        DeleteComment(videoRepository: videoRepository);
    GetComment getComment = GetComment(videoRepository: videoRepository);
    UpdateComment updateComment =
        UpdateComment(videoRepository: videoRepository);
    String mockData =
        '[{"id":43,"comment":"asd","name":"John Johnes","video_id":6,"user_id":4}]';

    setUp(() {
      dio = Dio();
      dioAdapter = DioAdapter(dio: dio);
      dio.httpClientAdapter = dioAdapter;
    });

    blocTest<CommentsCubit, CommentsState>(
      'emits success',
      act: (bloc) => bloc.loadComments(6),
      build: () => CommentsCubit(
          createComment: createComment,
          getComment: getComment,
          deleteComment: deleteComment,
          updateComment: updateComment),
      wait: const Duration(milliseconds: 5000),
      expect: () => [
        const CommentsLoadingState(comment: ''),
        CommentAddedState(comment: '',
            comments: (json.decode(mockData) as List).map((model) {
          CommentModel data = CommentModel.fromJson(model);
          return data;
        }).toList())
      ],
    );
  });
}
