import 'package:bloc_example/di/home/navigation_cubit/navigation_cubit.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
void main() {
  group('Navigation Bloc success scenarios: ', () {

    blocTest<NavigationCubit, NavigationSelection>(
      'emits to user',
      act: (bloc)=>bloc.changeSelection(NavigationSelection.user),
      build: () => NavigationCubit(),
      wait: const Duration(milliseconds: 1000),
      expect: () =>
          [NavigationSelection.user],
    );
    blocTest<NavigationCubit, NavigationSelection>(
      'emits to flow',
      act: (bloc)=>bloc.changeSelection(NavigationSelection.flow),
      build: () => NavigationCubit(),
      wait: const Duration(milliseconds: 1000),
      expect: () =>
          [NavigationSelection.flow],
    );
    blocTest<NavigationCubit, NavigationSelection>(
      'init state',
      build: () => NavigationCubit(),
      wait: const Duration(milliseconds: 1000),
      expect: () =>
          [],
    );
  });
}

Future<void> navigationSuccess() async {
  group('Navigation Bloc success scenarios: ', () {

    blocTest<NavigationCubit, NavigationSelection>(
      'emits to user',
      act: (bloc)=>bloc.changeSelection(NavigationSelection.user),
      build: () => NavigationCubit(),
      wait: const Duration(milliseconds: 1000),
      expect: () =>
          [NavigationSelection.user],
    );
    blocTest<NavigationCubit, NavigationSelection>(
      'emits to flow',
      act: (bloc)=>bloc.changeSelection(NavigationSelection.flow),
      build: () => NavigationCubit(),
      wait: const Duration(milliseconds: 1000),
      expect: () =>
          [NavigationSelection.flow],
    );
    blocTest<NavigationCubit, NavigationSelection>(
      'init state',
      build: () => NavigationCubit(),
      wait: const Duration(milliseconds: 1000),
      expect: () =>
          [],
    );
  });
}