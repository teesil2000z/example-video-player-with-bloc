import 'package:bloc_example/data/auth/datasourse/auth_local_data_source.dart';
import 'package:bloc_example/data/auth/datasourse/auth_remote_data_source.dart';
import 'package:bloc_example/data/auth/repositories/auth_repository.dart';
import 'package:bloc_example/data/auth/usecases/get_user.dart';
import 'package:bloc_example/data/video/datasourse/video_remote_data_source.dart';
import 'package:bloc_example/data/video/repositories/video_repository.dart';
import 'package:bloc_example/data/video/usecases/delete_video.dart';
import 'package:bloc_example/data/video/usecases/get_user_videos.dart';
import 'package:bloc_example/data/video/usecases/update_video.dart';
import 'package:bloc_example/di/home/user_bloc/user_bloc.dart';
import 'package:bloc_example/di/home/user_bloc/user_event.dart';
import 'package:bloc_example/di/home/user_bloc/user_state.dart';
import 'package:bloc_example/model/global_auth_state.dart';
import 'package:bloc_example/model/token_model.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http_mock_adapter/http_mock_adapter.dart';
import 'package:shared_preferences/shared_preferences.dart';
void main() async{
  SharedPreferences.setMockInitialValues({});
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  group('User Bloc Error scenarios: ', () {
    late Dio dio;
    late DioAdapter dioAdapter;
    GlobalAuthState globalAuthState = GlobalAuthState();
    globalAuthState.token = const TokenModel(
      token: '',
      id: 3, 
      name: 'test', 
      email: 'test@gmail.com'
    );
    AuthRemoteDataSource authRemoteDataSource = AuthRemoteDataSource(globalAuthState: globalAuthState);
    AuthLocalDataSource authLocalDataSource = AuthLocalDataSource(globalAuthState: globalAuthState, sharedPreferences: sharedPreferences);
    VideoRemoteDataSource videoRemoteDataSource = VideoRemoteDataSource(globalAuthState: globalAuthState);
    VideoRepository videoRepository = VideoRepository(remoteDataSource: videoRemoteDataSource);
    AuthRepository authRepository = AuthRepository(remoteDataSource: authRemoteDataSource, localDataSource: authLocalDataSource);
    DeleteVideo deleteVideo = DeleteVideo(videoRepository: videoRepository);
    GetUser getUser = GetUser(authRepository: authRepository);
    UpdateVideo updateVideo = UpdateVideo(videoRepository: videoRepository);
    GetUserVideos getUserVideos = GetUserVideos(videoRepository: videoRepository);

    setUp(() {
      dio = Dio();
      dioAdapter = DioAdapter(dio: dio);
      dio.httpClientAdapter = dioAdapter;
    });

    blocTest<UserBloc, UserState>(
      'emits failure',
      act: (bloc)=>bloc.add(LoadUserEvent()),
      build: () => UserBloc(
        updateVideo: updateVideo, 
        getUser: getUser, 
        getUserVideos: getUserVideos, 
        deleteVideo: deleteVideo
      ),
      wait: const Duration(milliseconds: 5000),
      expect: () =>
          [UserLoadingState(), UserErrorState()],
    );
  });
}

Future<void> userFailure() async{
  SharedPreferences.setMockInitialValues({});
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  group('User Bloc Error scenarios: ', () {
    late Dio dio;
    late DioAdapter dioAdapter;
    GlobalAuthState globalAuthState = GlobalAuthState();
    globalAuthState.token = const TokenModel(
      token: '',
      id: 3, 
      name: 'test', 
      email: 'test@gmail.com'
    );
    AuthRemoteDataSource authRemoteDataSource = AuthRemoteDataSource(globalAuthState: globalAuthState);
    AuthLocalDataSource authLocalDataSource = AuthLocalDataSource(globalAuthState: globalAuthState, sharedPreferences: sharedPreferences);
    VideoRemoteDataSource videoRemoteDataSource = VideoRemoteDataSource(globalAuthState: globalAuthState);
    VideoRepository videoRepository = VideoRepository(remoteDataSource: videoRemoteDataSource);
    AuthRepository authRepository = AuthRepository(remoteDataSource: authRemoteDataSource, localDataSource: authLocalDataSource);
    DeleteVideo deleteVideo = DeleteVideo(videoRepository: videoRepository);
    GetUser getUser = GetUser(authRepository: authRepository);
    UpdateVideo updateVideo = UpdateVideo(videoRepository: videoRepository);
    GetUserVideos getUserVideos = GetUserVideos(videoRepository: videoRepository);

    setUp(() {
      dio = Dio();
      dioAdapter = DioAdapter(dio: dio);
      dio.httpClientAdapter = dioAdapter;
    });

    blocTest<UserBloc, UserState>(
      'emits failure',
      act: (bloc)=>bloc.add(LoadUserEvent()),
      build: () => UserBloc(
        updateVideo: updateVideo, 
        getUser: getUser, 
        getUserVideos: getUserVideos, 
        deleteVideo: deleteVideo
      ),
      wait: const Duration(milliseconds: 5000),
      expect: () =>
          [UserLoadingState(), UserErrorState()],
    );
  });
}