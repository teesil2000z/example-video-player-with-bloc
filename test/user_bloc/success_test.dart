import 'dart:convert';

import 'package:bloc_example/data/auth/datasourse/auth_local_data_source.dart';
import 'package:bloc_example/data/auth/datasourse/auth_remote_data_source.dart';
import 'package:bloc_example/data/auth/repositories/auth_repository.dart';
import 'package:bloc_example/data/auth/usecases/get_user.dart';
import 'package:bloc_example/data/video/datasourse/video_remote_data_source.dart';
import 'package:bloc_example/data/video/repositories/video_repository.dart';
import 'package:bloc_example/data/video/usecases/delete_video.dart';
import 'package:bloc_example/data/video/usecases/get_user_videos.dart';
import 'package:bloc_example/data/video/usecases/update_video.dart';
import 'package:bloc_example/di/home/user_bloc/user_bloc.dart';
import 'package:bloc_example/di/home/user_bloc/user_event.dart';
import 'package:bloc_example/di/home/user_bloc/user_state.dart';
import 'package:bloc_example/model/global_auth_state.dart';
import 'package:bloc_example/model/token_model.dart';
import 'package:bloc_example/model/user_data.dart';
import 'package:bloc_example/model/video_model.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http_mock_adapter/http_mock_adapter.dart';
import 'package:shared_preferences/shared_preferences.dart';
void main() async{
  SharedPreferences.setMockInitialValues({});
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  group('User Bloc Success scenarios: ', () {
    late Dio dio;
    late DioAdapter dioAdapter;
    GlobalAuthState globalAuthState = GlobalAuthState();
    globalAuthState.token = const TokenModel(
      token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IjEyMzEyMzEyMyIsImVtYWlsIjoiampAZ21haWwuY29tIiwiZXhwIjoxNjc0ODgyNjYxfQ.JvxSIlZ1-xQV9I0XXN4WDY5zh6EF3T6Z6ehgGcQjw8E",
      id: 3, 
      name: 'test', 
      email: 'test@gmail.com'
    );
    AuthRemoteDataSource authRemoteDataSource = AuthRemoteDataSource(globalAuthState: globalAuthState);
    AuthLocalDataSource authLocalDataSource = AuthLocalDataSource(globalAuthState: globalAuthState, sharedPreferences: sharedPreferences);
    VideoRemoteDataSource videoRemoteDataSource = VideoRemoteDataSource(globalAuthState: globalAuthState);
    VideoRepository videoRepository = VideoRepository(remoteDataSource: videoRemoteDataSource);
    AuthRepository authRepository = AuthRepository(remoteDataSource: authRemoteDataSource, localDataSource: authLocalDataSource);
    DeleteVideo deleteVideo = DeleteVideo(videoRepository: videoRepository);
    GetUser getUser = GetUser(authRepository: authRepository);
    UpdateVideo updateVideo = UpdateVideo(videoRepository: videoRepository);
    GetUserVideos getUserVideos = GetUserVideos(videoRepository: videoRepository);
    String mockData = '[{"id":1,"name":"Sun","description":"Sun storm","path":"https://firebasestorage.googleapis.com/v0/b/test-task-67bb2.appspot.com/o/wild_4.mp4?alt=media\u0026token=8cd085ec-5f46-4b4a-8ee7-255c8e14d679","user_id":3},{"id":2,"name":"Forest","description":"Forest 1","path":"https://firebasestorage.googleapis.com/v0/b/test-task-67bb2.appspot.com/o/wild_2.mp4?alt=media\u0026token=1a708a0d-950b-4bd3-b1ae-6213fc1142ad","user_id":3},{"id":3,"name":"Forest","description":"Forest 2","path":"https://firebasestorage.googleapis.com/v0/b/test-task-67bb2.appspot.com/o/timer_3.mp4?alt=media\u0026token=e15c48db-be1e-46d0-9177-7d3209c53bf6","user_id":3},{"id":4,"name":"Forest","description":"Forest 3","path":"https://firebasestorage.googleapis.com/v0/b/test-task-67bb2.appspot.com/o/timer_1.mp4?alt=media\u0026token=d4a7aed5-bacf-43a6-9162-9bfd29908b81","user_id":3}]';

    setUp(() {
      dio = Dio();
      dioAdapter = DioAdapter(dio: dio);
      dio.httpClientAdapter = dioAdapter;
    });

    blocTest<UserBloc, UserState>(
      'emits success',
      act: (bloc)=>bloc.add(LoadUserEvent()),
      build: () => UserBloc(
        updateVideo: updateVideo, 
        getUser: getUser, 
        getUserVideos: getUserVideos, 
        deleteVideo: deleteVideo
      ),
      wait: const Duration(milliseconds: 5000),
      expect: () =>
          [UserLoadingState(), UserLoadedState(
            userData: const UserData(email: '', name: '', id: 0), 
            videos: (json.decode(mockData) as List)
                .map((data) => VideoModel.fromJson(data))
                .toList()
          )],
    );
  });
}

Future<void> userSuccess() async{
  SharedPreferences.setMockInitialValues({});
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  group('User Bloc Success scenarios: ', () {
    late Dio dio;
    late DioAdapter dioAdapter;
    GlobalAuthState globalAuthState = GlobalAuthState();
    globalAuthState.token = const TokenModel(
      token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IjEyMzEyMzEyMyIsImVtYWlsIjoiampAZ21haWwuY29tIiwiZXhwIjoxNjc0ODgyNjYxfQ.JvxSIlZ1-xQV9I0XXN4WDY5zh6EF3T6Z6ehgGcQjw8E",
      id: 3, 
      name: 'test', 
      email: 'test@gmail.com'
    );
    AuthRemoteDataSource authRemoteDataSource = AuthRemoteDataSource(globalAuthState: globalAuthState);
    AuthLocalDataSource authLocalDataSource = AuthLocalDataSource(globalAuthState: globalAuthState, sharedPreferences: sharedPreferences);
    VideoRemoteDataSource videoRemoteDataSource = VideoRemoteDataSource(globalAuthState: globalAuthState);
    VideoRepository videoRepository = VideoRepository(remoteDataSource: videoRemoteDataSource);
    AuthRepository authRepository = AuthRepository(remoteDataSource: authRemoteDataSource, localDataSource: authLocalDataSource);
    DeleteVideo deleteVideo = DeleteVideo(videoRepository: videoRepository);
    GetUser getUser = GetUser(authRepository: authRepository);
    UpdateVideo updateVideo = UpdateVideo(videoRepository: videoRepository);
    GetUserVideos getUserVideos = GetUserVideos(videoRepository: videoRepository);
    String mockData = '[{"id":1,"name":"Sun","description":"Sun storm","path":"https://firebasestorage.googleapis.com/v0/b/test-task-67bb2.appspot.com/o/wild_4.mp4?alt=media\u0026token=8cd085ec-5f46-4b4a-8ee7-255c8e14d679","user_id":3},{"id":2,"name":"Forest","description":"Forest 1","path":"https://firebasestorage.googleapis.com/v0/b/test-task-67bb2.appspot.com/o/wild_2.mp4?alt=media\u0026token=1a708a0d-950b-4bd3-b1ae-6213fc1142ad","user_id":3},{"id":3,"name":"Forest","description":"Forest 2","path":"https://firebasestorage.googleapis.com/v0/b/test-task-67bb2.appspot.com/o/timer_3.mp4?alt=media\u0026token=e15c48db-be1e-46d0-9177-7d3209c53bf6","user_id":3},{"id":4,"name":"Forest","description":"Forest 3","path":"https://firebasestorage.googleapis.com/v0/b/test-task-67bb2.appspot.com/o/timer_1.mp4?alt=media\u0026token=d4a7aed5-bacf-43a6-9162-9bfd29908b81","user_id":3}]';

    setUp(() {
      dio = Dio();
      dioAdapter = DioAdapter(dio: dio);
      dio.httpClientAdapter = dioAdapter;
    });

    blocTest<UserBloc, UserState>(
      'emits success',
      act: (bloc)=>bloc.add(LoadUserEvent()),
      build: () => UserBloc(
        updateVideo: updateVideo, 
        getUser: getUser, 
        getUserVideos: getUserVideos, 
        deleteVideo: deleteVideo
      ),
      wait: const Duration(milliseconds: 5000),
      expect: () =>
          [UserLoadingState(), UserLoadedState(
            userData: const UserData(email: '', name: '', id: 0), 
            videos: (json.decode(mockData) as List)
                .map((data) => VideoModel.fromJson(data))
                .toList()
          )],
    );
  });
}