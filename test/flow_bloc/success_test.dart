import 'dart:convert';

import 'package:bloc_example/data/video/datasourse/video_remote_data_source.dart';
import 'package:bloc_example/data/video/repositories/video_repository.dart';
import 'package:bloc_example/data/video/usecases/all_videos.dart';
import 'package:bloc_example/di/home/flow_bloc/flow_bloc.dart';
import 'package:bloc_example/di/home/flow_bloc/flow_event.dart';
import 'package:bloc_example/di/home/flow_bloc/flow_state.dart';
import 'package:bloc_example/model/global_auth_state.dart';
import 'package:bloc_example/model/token_model.dart';
import 'package:bloc_example/model/video_model.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http_mock_adapter/http_mock_adapter.dart';
void main(){
  group('Flow Bloc Success scenarios: ', () {
    late Dio dio;
    late DioAdapter dioAdapter;
    GlobalAuthState globalAuthState = GlobalAuthState();
    globalAuthState.token = const TokenModel(
      token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IjEyMzEyMzEyMyIsImVtYWlsIjoiampAZ21haWwuY29tIiwiZXhwIjoxNjc0ODgyNjYxfQ.JvxSIlZ1-xQV9I0XXN4WDY5zh6EF3T6Z6ehgGcQjw8E",
      id: 1, 
      name: 'test', 
      email: 'test@gmail.com'
    );
    VideoRemoteDataSource videoRemoteDataSource = VideoRemoteDataSource(globalAuthState: globalAuthState);
    VideoRepository videoRepository = VideoRepository(remoteDataSource: videoRemoteDataSource);
    AllVideos allVideos = AllVideos(videoRepository: videoRepository);
    String mockData = '[{"id":5,"name":"Timer","description":"Timer 1","path":"https://firebasestorage.googleapis.com/v0/b/test-task-67bb2.appspot.com/o/timer_1.mp4?alt=media\u0026token=d4a7aed5-bacf-43a6-9162-9bfd29908b81","user_id":4},{"id":6,"name":"Timer","description":"Timer 2","path":"https://firebasestorage.googleapis.com/v0/b/test-task-67bb2.appspot.com/o/timer_2.mp4?alt=media\u0026token=04db4cf8-4707-4161-b74b-aa76b47baa85","user_id":4},{"id":7,"name":"Timer","description":"Timer 3","path":"https://firebasestorage.googleapis.com/v0/b/test-task-67bb2.appspot.com/o/timer_3.mp4?alt=media\u0026token=e15c48db-be1e-46d0-9177-7d3209c53bf6","user_id":4},{"id":8,"name":"Timer","description":"Timer 4","path":"https://firebasestorage.googleapis.com/v0/b/test-task-67bb2.appspot.com/o/timer_4.mp4?alt=media\u0026token=14c1cc93-3ca6-4b1a-8e30-64fdf722bf7c","user_id":4},{"id":19,"name":"Example","description":"load after update","path":"https://firebasestorage.googleapis.com/v0/b/test-task-67bb2.appspot.com/o/trim.9135A834-0FC4-49EA-8518-C6BCFF6FD2AB.MOV?alt=media\u0026token=5e121687-ea9e-4568-bcf5-e6555a862d13","user_id":4},{"id":17,"name":"Update","description":"test2","path":"https://firebasestorage.googleapis.com/v0/b/test-task-67bb2.appspot.com/o/trim.EDEEAC56-D230-40AE-8351-88AA13E0EC59.MOV?alt=media\u0026token=921f6f01-1735-4a85-95b2-f7c8c2a59e6f","user_id":4},{"id":16,"name":"City footage","description":"city footage 1","path":"https://firebasestorage.googleapis.com/v0/b/test-task-67bb2.appspot.com/o/trim.C9E1CA8A-1499-4CA1-BDCC-6301B836D46C.MOV?alt=media\u0026token=d67c38bb-e647-490f-9fc9-cee0af3fe41f","user_id":4},{"id":1,"name":"Sun","description":"Sun storm","path":"https://firebasestorage.googleapis.com/v0/b/test-task-67bb2.appspot.com/o/wild_4.mp4?alt=media\u0026token=8cd085ec-5f46-4b4a-8ee7-255c8e14d679","user_id":3},{"id":2,"name":"Forest","description":"Forest 1","path":"https://firebasestorage.googleapis.com/v0/b/test-task-67bb2.appspot.com/o/wild_2.mp4?alt=media\u0026token=1a708a0d-950b-4bd3-b1ae-6213fc1142ad","user_id":3},{"id":3,"name":"Forest","description":"Forest 2","path":"https://firebasestorage.googleapis.com/v0/b/test-task-67bb2.appspot.com/o/timer_3.mp4?alt=media\u0026token=e15c48db-be1e-46d0-9177-7d3209c53bf6","user_id":3},{"id":4,"name":"Forest","description":"Forest 3","path":"https://firebasestorage.googleapis.com/v0/b/test-task-67bb2.appspot.com/o/timer_1.mp4?alt=media\u0026token=d4a7aed5-bacf-43a6-9162-9bfd29908b81","user_id":3}]';
    setUp(() {
      dio = Dio();
      dioAdapter = DioAdapter(dio: dio);
      dio.httpClientAdapter = dioAdapter;
    });

    blocTest<FlowBloc, FlowState>(
      'emits success',
      act: (bloc)=>bloc.add(FlowLoadEvent()),
      build: () => FlowBloc(allVideos: allVideos),
      wait: const Duration(milliseconds: 5000),
      expect: () =>
          [FlowLoadingState(), FlowLoadedState(data: (json.decode(mockData) as List)
                .map((data) => VideoModel.fromJson(data))
                .toList())],
    );
  });
}

Future<void> flowSuccess() async {
  group('Flow Bloc Success scenarios: ', () {
    late Dio dio;
    late DioAdapter dioAdapter;
    GlobalAuthState globalAuthState = GlobalAuthState();
    globalAuthState.token = const TokenModel(
      token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IjEyMzEyMzEyMyIsImVtYWlsIjoiampAZ21haWwuY29tIiwiZXhwIjoxNjc0ODgyNjYxfQ.JvxSIlZ1-xQV9I0XXN4WDY5zh6EF3T6Z6ehgGcQjw8E",
      id: 1, 
      name: 'test', 
      email: 'test@gmail.com'
    );
    VideoRemoteDataSource videoRemoteDataSource = VideoRemoteDataSource(globalAuthState: globalAuthState);
    VideoRepository videoRepository = VideoRepository(remoteDataSource: videoRemoteDataSource);
    AllVideos allVideos = AllVideos(videoRepository: videoRepository);
    String mockData = '[{"id":5,"name":"Timer","description":"Timer 1","path":"https://firebasestorage.googleapis.com/v0/b/test-task-67bb2.appspot.com/o/timer_1.mp4?alt=media\u0026token=d4a7aed5-bacf-43a6-9162-9bfd29908b81","user_id":4},{"id":6,"name":"Timer","description":"Timer 2","path":"https://firebasestorage.googleapis.com/v0/b/test-task-67bb2.appspot.com/o/timer_2.mp4?alt=media\u0026token=04db4cf8-4707-4161-b74b-aa76b47baa85","user_id":4},{"id":7,"name":"Timer","description":"Timer 3","path":"https://firebasestorage.googleapis.com/v0/b/test-task-67bb2.appspot.com/o/timer_3.mp4?alt=media\u0026token=e15c48db-be1e-46d0-9177-7d3209c53bf6","user_id":4},{"id":8,"name":"Timer","description":"Timer 4","path":"https://firebasestorage.googleapis.com/v0/b/test-task-67bb2.appspot.com/o/timer_4.mp4?alt=media\u0026token=14c1cc93-3ca6-4b1a-8e30-64fdf722bf7c","user_id":4},{"id":19,"name":"Example","description":"load after update","path":"https://firebasestorage.googleapis.com/v0/b/test-task-67bb2.appspot.com/o/trim.9135A834-0FC4-49EA-8518-C6BCFF6FD2AB.MOV?alt=media\u0026token=5e121687-ea9e-4568-bcf5-e6555a862d13","user_id":4},{"id":17,"name":"Update","description":"test2","path":"https://firebasestorage.googleapis.com/v0/b/test-task-67bb2.appspot.com/o/trim.EDEEAC56-D230-40AE-8351-88AA13E0EC59.MOV?alt=media\u0026token=921f6f01-1735-4a85-95b2-f7c8c2a59e6f","user_id":4},{"id":16,"name":"City footage","description":"city footage 1","path":"https://firebasestorage.googleapis.com/v0/b/test-task-67bb2.appspot.com/o/trim.C9E1CA8A-1499-4CA1-BDCC-6301B836D46C.MOV?alt=media\u0026token=d67c38bb-e647-490f-9fc9-cee0af3fe41f","user_id":4},{"id":1,"name":"Sun","description":"Sun storm","path":"https://firebasestorage.googleapis.com/v0/b/test-task-67bb2.appspot.com/o/wild_4.mp4?alt=media\u0026token=8cd085ec-5f46-4b4a-8ee7-255c8e14d679","user_id":3},{"id":2,"name":"Forest","description":"Forest 1","path":"https://firebasestorage.googleapis.com/v0/b/test-task-67bb2.appspot.com/o/wild_2.mp4?alt=media\u0026token=1a708a0d-950b-4bd3-b1ae-6213fc1142ad","user_id":3},{"id":3,"name":"Forest","description":"Forest 2","path":"https://firebasestorage.googleapis.com/v0/b/test-task-67bb2.appspot.com/o/timer_3.mp4?alt=media\u0026token=e15c48db-be1e-46d0-9177-7d3209c53bf6","user_id":3},{"id":4,"name":"Forest","description":"Forest 3","path":"https://firebasestorage.googleapis.com/v0/b/test-task-67bb2.appspot.com/o/timer_1.mp4?alt=media\u0026token=d4a7aed5-bacf-43a6-9162-9bfd29908b81","user_id":3}]';
    setUp(() {
      dio = Dio();
      dioAdapter = DioAdapter(dio: dio);
      dio.httpClientAdapter = dioAdapter;
    });

    blocTest<FlowBloc, FlowState>(
      'emits success',
      act: (bloc)=>bloc.add(FlowLoadEvent()),
      build: () => FlowBloc(allVideos: allVideos),
      wait: const Duration(milliseconds: 5000),
      expect: () =>
          [FlowLoadingState(), FlowLoadedState(data: (json.decode(mockData) as List)
                .map((data) => VideoModel.fromJson(data))
                .toList())],
    );
  });
}