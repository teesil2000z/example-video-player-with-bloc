import 'package:bloc_example/data/video/datasourse/video_remote_data_source.dart';
import 'package:bloc_example/data/video/repositories/video_repository.dart';
import 'package:bloc_example/data/video/usecases/all_videos.dart';
import 'package:bloc_example/di/home/flow_bloc/flow_bloc.dart';
import 'package:bloc_example/di/home/flow_bloc/flow_event.dart';
import 'package:bloc_example/di/home/flow_bloc/flow_state.dart';
import 'package:bloc_example/model/global_auth_state.dart';
import 'package:bloc_example/model/token_model.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http_mock_adapter/http_mock_adapter.dart';
void main(){
  group('Flow Bloc Error scenarios: ', () {
    late Dio dio;
    late DioAdapter dioAdapter;
    GlobalAuthState globalAuthState = GlobalAuthState();
    globalAuthState.token = const TokenModel(
      token: '',
      id: 1, 
      name: 'test', 
      email: 'test@gmail.com'
    );
    VideoRemoteDataSource videoRemoteDataSource = VideoRemoteDataSource(globalAuthState: globalAuthState);
    VideoRepository videoRepository = VideoRepository(remoteDataSource: videoRemoteDataSource);
    AllVideos allVideos = AllVideos(videoRepository: videoRepository);

    setUp(() {
      dio = Dio();
      dioAdapter = DioAdapter(dio: dio);
      dio.httpClientAdapter = dioAdapter;
    });

    blocTest<FlowBloc, FlowState>(
      'emits failure',
      act: (bloc)=>bloc.add(FlowLoadEvent()),
      build: () => FlowBloc(allVideos: allVideos),
      wait: const Duration(milliseconds: 5000),
      expect: () =>
          [FlowLoadingState(), FlowErrorState()],
    );
  });
}

Future<void> flowFailure() async{
  group('Flow Bloc Error scenarios: ', () {
    late Dio dio;
    late DioAdapter dioAdapter;
    GlobalAuthState globalAuthState = GlobalAuthState();
    globalAuthState.token = const TokenModel(
      token: '',
      id: 1, 
      name: 'test', 
      email: 'test@gmail.com'
    );
    VideoRemoteDataSource videoRemoteDataSource = VideoRemoteDataSource(globalAuthState: globalAuthState);
    VideoRepository videoRepository = VideoRepository(remoteDataSource: videoRemoteDataSource);
    AllVideos allVideos = AllVideos(videoRepository: videoRepository);

    setUp(() {
      dio = Dio();
      dioAdapter = DioAdapter(dio: dio);
      dio.httpClientAdapter = dioAdapter;
    });

    blocTest<FlowBloc, FlowState>(
      'emits failure',
      act: (bloc)=>bloc.add(FlowLoadEvent()),
      build: () => FlowBloc(allVideos: allVideos),
      wait: const Duration(milliseconds: 5000),
      expect: () =>
          [FlowLoadingState(), FlowErrorState()],
    );
  });
}