import 'dart:convert';

import 'package:bloc_example/data/constants.dart';
import 'package:bloc_example/model/errors.dart';
import 'package:bloc_example/model/global_auth_state.dart';
import 'package:bloc_example/model/token_model.dart';
import 'package:dio/dio.dart';

abstract interface class IAuthRemoteDataSource{
  Future<TokenModel> auth(String email, String password);

  Future<bool> signUp(String name, String email, String password);
}

class AuthRemoteDataSource implements IAuthRemoteDataSource{
  final GlobalAuthState globalAuthState;
  AuthRemoteDataSource({required this.globalAuthState});

  @override
  Future<TokenModel> auth(String email, String password) async{
    try{
      final response = await Dio().post(
        '$domain/auth',
        options: Options(headers: {'Content-Type': 'application/json'}),
        data: jsonEncode({
          "email": email,
          "password": password
        })
      );
      TokenModel token = TokenModel.fromJson(json.decode(response.data));
      globalAuthState.token = token;
      return token;
    }catch(e){
      if(e is DioError){
        throw ServerException(statusCode: e.response!.statusCode, stackTrace: e.stackTrace.toString());
      }else{
        throw CacheExceprion(stackTrace: (e as Error).stackTrace.toString());
      }
    }
  }

  @override
  Future<bool> signUp(String name, String email, String password) async{
    try{
      final response = await Dio().post(
        '$domain/sign_up',
        options: Options(headers: {'Content-Type': 'application/json'}),
        data: jsonEncode({
          "email": email,
          "password": password,
          "name": name
        })
      );
      return json.decode(response.data);
    }catch(e){
      if(e is DioError){
        throw ServerException(statusCode: e.response!.statusCode, stackTrace: e.stackTrace.toString());
      }else{
        throw CacheExceprion(stackTrace: (e as Error).stackTrace.toString());
      }
    }
  }
  
}