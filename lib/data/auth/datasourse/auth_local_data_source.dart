import 'package:bloc_example/model/global_auth_state.dart';
import 'package:bloc_example/model/token_model.dart';
import 'package:bloc_example/model/user_data.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract interface class IAuthLocalDataSource{
  Future<void> saveToken(TokenModel token);

  Future<TokenModel> getToken();

  Future<void> deleteToken();

  Future<UserData> getUser();
}


const token = 'TOKEN';
const name = 'NAME';
const userId = 'ID';
const email = 'EMAIL';


class AuthLocalDataSource implements IAuthLocalDataSource{
  final SharedPreferences sharedPreferences;
  final GlobalAuthState globalAuthState;

  AuthLocalDataSource(
    {
      required this.globalAuthState,
      required this.sharedPreferences
    }
  );

  @override
  Future<void> deleteToken() async{
    globalAuthState.token = null;
    await sharedPreferences.setString(token, '');
    await sharedPreferences.setString(name, '');
    await sharedPreferences.setString(email, '');
    await sharedPreferences.setInt(userId, 0);
  }

  @override
  Future<TokenModel> getToken() async{
    final tokenPref = sharedPreferences.getString(token) ?? '';
    final namePref = sharedPreferences.getString(name) ?? '';
    final emailPref = sharedPreferences.getString(email) ?? '';
    final idPref = sharedPreferences.getInt(userId) ?? 0;
    TokenModel model = TokenModel(
      token: tokenPref, 
      id: idPref, 
      name: namePref,
      email: emailPref
    );
    globalAuthState.token = model;
    return model;
  }

  @override
  // ignore: avoid_renaming_method_parameters
  Future<void> saveToken(TokenModel data) async{
    globalAuthState.token = data;
    await sharedPreferences.setString(token, data.token);
    await sharedPreferences.setString(name, data.name);
    await sharedPreferences.setInt(userId, data.id);
    await sharedPreferences.setString(email, data.email);
  }
  
  @override
  Future<UserData> getUser() async {
    final namePref = sharedPreferences.getString(name) ?? '';
    final emailPref = sharedPreferences.getString(email) ?? '';
    final idPref = sharedPreferences.getInt(userId) ?? 0;
    return UserData(
      email: emailPref, 
      name: namePref, 
      id: idPref
    );
  }

}