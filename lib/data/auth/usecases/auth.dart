
import 'package:bloc_example/data/auth/repositories/auth_repository.dart';
import 'package:bloc_example/model/auth_params.dart';
import 'package:bloc_example/model/errors.dart';
import 'package:bloc_example/model/token_model.dart';
import 'package:bloc_example/model/use_case.dart';
import 'package:dartz/dartz.dart';

class Auth implements UseCase<TokenModel, AuthParams> {
  final IAuthRepository authRepository;

  Auth({required this.authRepository});

  @override
  Future<Either<Failure, TokenModel>> call(params) async{
    return await authRepository.auth(params!.email, params.password);
  }

}