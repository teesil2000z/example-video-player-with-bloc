import 'package:bloc_example/data/auth/repositories/auth_repository.dart';
import 'package:bloc_example/model/errors.dart';
import 'package:bloc_example/model/sign_up_params.dart';
import 'package:bloc_example/model/use_case.dart';
import 'package:dartz/dartz.dart';

class SignUp implements UseCase<bool, SignUpParams> {
  final IAuthRepository authRepository;

  SignUp({required this.authRepository});

  @override
  Future<Either<Failure, bool>> call(params) async{
    return await authRepository.signUp(params!.name, params.email, params.password);
  }

}