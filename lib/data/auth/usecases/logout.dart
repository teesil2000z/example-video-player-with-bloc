import 'package:bloc_example/data/auth/repositories/auth_repository.dart';
import 'package:bloc_example/model/errors.dart';
import 'package:bloc_example/model/use_case.dart';
import 'package:dartz/dartz.dart';

class Logout implements UseCase<bool, dynamic> {
  final IAuthRepository authRepository;

  Logout({required this.authRepository});

  @override
  Future<Either<Failure, bool>> call(params) async{
    return await authRepository.logout();
  }

}