import 'package:bloc_example/data/auth/repositories/auth_repository.dart';
import 'package:bloc_example/model/errors.dart';
import 'package:bloc_example/model/use_case.dart';
import 'package:bloc_example/model/user_data.dart';
import 'package:dartz/dartz.dart';

class GetUser implements UseCase<UserData, dynamic> {
  final IAuthRepository authRepository;

  GetUser({required this.authRepository});

  @override
  Future<Either<Failure, UserData>> call(params) async{
    return await authRepository.getUser();
  }

}