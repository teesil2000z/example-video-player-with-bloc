import 'package:bloc_example/data/auth/datasourse/auth_local_data_source.dart';
import 'package:bloc_example/data/auth/datasourse/auth_remote_data_source.dart';
import 'package:bloc_example/model/errors.dart';
import 'package:bloc_example/model/token_model.dart';
import 'package:bloc_example/model/user_data.dart';
import 'package:dartz/dartz.dart';

abstract interface class IAuthRepository {
  Future<Either<Failure, TokenModel>> auth(String email, String password);

  Future<Either<Failure, bool>> signUp(
      String name, String email, String password);

  Future<Either<Failure, bool>> logout();

  Future<Either<Failure, UserData>> getUser();
}

class AuthRepository implements IAuthRepository {
  final IAuthRemoteDataSource remoteDataSource;
  final IAuthLocalDataSource localDataSource;
  AuthRepository(
      {required this.remoteDataSource, required this.localDataSource});

  @override
  Future<Either<Failure, TokenModel>> auth(
      String email, String password) async {
    try {
      final token = await remoteDataSource.auth(email, password);
      await localDataSource.saveToken(token);
      return Right(token);
    } catch (e) {
      if (e is ServerException) {
        return Left(ServerFailure(statusCode: e.statusCode));
      } else {
        return const Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, bool>> signUp(
      String name, String email, String password) async {
    try {
      final result = await remoteDataSource.signUp(name, email, password);
      return Right(result);
    } catch (e) {
      if (e is ServerException) {
        return Left(ServerFailure(statusCode: e.statusCode));
      } else {
        return const Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, bool>> logout() async {
    localDataSource.deleteToken();
    return const Right(true);
  }
  
  @override
  Future<Either<Failure, UserData>> getUser() async{
    final data = await localDataSource.getUser();
    return Right(data);
  }
}
