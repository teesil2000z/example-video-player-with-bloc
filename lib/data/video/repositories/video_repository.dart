
import 'package:bloc_example/data/video/datasourse/video_remote_data_source.dart';
import 'package:bloc_example/model/comment_model.dart';
import 'package:bloc_example/model/errors.dart';
import 'package:bloc_example/model/video_model.dart';
import 'package:dartz/dartz.dart';

abstract interface class IVideoRepository {
  Future<Either<Failure, List<VideoModel>>> allVideos();

  Future<Either<Failure, bool>> deleteComment(int commentId);

  Future<Either<Failure, List<VideoModel>>> getRelatedVideos(int id);

  Future<Either<Failure, VideoModel>> getVideo(int id);

  Future<Either<Failure, bool>> createComment(int id, String comment, int videoId);

  Future<Either<Failure, List<CommentModel>>> getComments(int id);

  Future<Either<Failure, bool>> createVideo(String name, String description, String path);

  Future<Either<Failure, List<VideoModel>>> getUserVideos();

  Future<Either<Failure, List<CommentModel>>> updateComment(int id, String comment, int videoId);

  Future<Either<Failure, bool>> deleteVideo(int videoId);

  Future<Either<Failure, List<VideoModel>>> updateVideo(int id, String name, String description);

}

class VideoRepository implements IVideoRepository{
  final IVideoRemoteDataSource remoteDataSource;

  VideoRepository({required this.remoteDataSource});

  @override
  Future<Either<Failure, List<VideoModel>>> allVideos() async{
    try{
      final videos = await remoteDataSource.allVideos();
      return Right(videos);
    }catch(e){
      if(e is ServerException){
        return Left(ServerFailure(statusCode: e.statusCode));
      }else{
        return const Left(CacheFailure());
      }
    }
  }
  
  @override
  Future<Either<Failure, bool>> createComment(int id, String comment, int videoId) async {
    try{
      final comments = await remoteDataSource.createComment(id, comment, videoId);
      return Right(comments);
    }catch(e){
      if(e is ServerException){
        return Left(ServerFailure(statusCode: e.statusCode));
      }else{
        return const Left(CacheFailure());
      }
    }
  }
  
  @override
  Future<Either<Failure, List<CommentModel>>> getComments(int id) async{
    try{
      final comments = await remoteDataSource.getComments(id);
      return Right(comments);
    }catch(e){
      if(e is ServerException){
        return Left(ServerFailure(statusCode: e.statusCode));
      }else{
        return const Left(CacheFailure());
      }
    }
  }
  
  @override
  Future<Either<Failure, bool>> createVideo(String name, String description, String path) async{
    try{
      final result = await remoteDataSource.createVideo(name, description, path);
      return Right(result);
    }catch(e){
      if(e is ServerException){
        return Left(ServerFailure(statusCode: e.statusCode));
      }else{
        return const Left(CacheFailure());
      }
    }
  }
  
  @override
  Future<Either<Failure, List<VideoModel>>> getUserVideos() async{
    try{
      final videos = await remoteDataSource.getUserVideos();
      return Right(videos);
    }catch(e){
      if(e is ServerException){
        return Left(ServerFailure(statusCode: e.statusCode));
      }else{
        return const Left(CacheFailure());
      }
    }
  }
  
  @override
  Future<Either<Failure, List<VideoModel>>> getRelatedVideos(int id) async{
    try{
      final videos = await remoteDataSource.getRelatedVideos(id);
      return Right(videos);
    }catch(e){
      if(e is ServerException){
        return Left(ServerFailure(statusCode: e.statusCode));
      }else{
        return const Left(CacheFailure());
      }
    }
  }
  
  @override
  Future<Either<Failure, bool>> deleteComment(int commentId) async{
    try{
      final result = await remoteDataSource.deleteComment(commentId);
      return Right(result);
    }catch(e){
      if(e is ServerException){
        return Left(ServerFailure(statusCode: e.statusCode));
      }else{
        return const Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, bool>> deleteVideo(int commentId) async{
    try{
      final result = await remoteDataSource.deleteVideo(commentId);
      return Right(result);
    }catch(e){
      if(e is ServerException){
        return Left(ServerFailure(statusCode: e.statusCode));
      }else{
        return const Left(CacheFailure());
      }
    }
  }
  
  @override
  Future<Either<Failure, List<CommentModel>>> updateComment(int id, String comment, int videoId) async{
   try{
      final result = await remoteDataSource.updateComment(id, comment, videoId);
      return Right(result);
    }catch(e){
      if(e is ServerException){
        return Left(ServerFailure(statusCode: e.statusCode));
      }else{
        return const Left(CacheFailure());
      }
    }
  }
  
  @override
  Future<Either<Failure, List<VideoModel>>> updateVideo(int id, String name, String description) async {
    try{
      final result = await remoteDataSource.updateVideo(id, name, description);
      return Right(result);
    }catch(e){
      if(e is ServerException){
        return Left(ServerFailure(statusCode: e.statusCode));
      }else{
        return const Left(CacheFailure());
      }
    }
  }
  
  @override
  Future<Either<Failure, VideoModel>> getVideo(int id) async{
    try{
      final result = await remoteDataSource.getVideo(id);
      return Right(result);
    }catch(e){
      if(e is ServerException){
        return Left(ServerFailure(statusCode: e.statusCode));
      }else{
        return const Left(CacheFailure());
      }
    }
  }

}