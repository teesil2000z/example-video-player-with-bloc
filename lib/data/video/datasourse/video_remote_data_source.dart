import 'dart:convert';

import 'package:bloc_example/model/comment_model.dart';
import 'package:bloc_example/model/errors.dart';
import 'package:bloc_example/model/global_auth_state.dart';
import 'package:bloc_example/model/video_model.dart';
import 'package:dio/dio.dart';
import 'package:bloc_example/data/constants.dart';

abstract interface class IVideoRemoteDataSource {
  Future<List<VideoModel>> allVideos();

  Future<bool> deleteComment(int commentId);

  Future<List<VideoModel>> getRelatedVideos(int id);

  Future<bool> createComment(int id, String comment, int videoId);

  Future<List<CommentModel>> getComments(int id);

  Future<bool> createVideo(String name, String description, String path);

  Future<VideoModel> getVideo(int id);

  Future<List<VideoModel>> getUserVideos();

  Future<List<CommentModel>> updateComment(int id, String comment, int videoId);

  Future<bool> deleteVideo(int videoId);

  Future<List<VideoModel>> updateVideo(int id, String name, String description);
}

class VideoRemoteDataSource implements IVideoRemoteDataSource {
  final GlobalAuthState globalAuthState;
  VideoRemoteDataSource({required this.globalAuthState});

  @override
  Future<List<VideoModel>> allVideos() async {
    try {
      final response = await Dio().get(
        '$domain/videos',
        options: Options(headers: {
          'Content-Type': 'application/json',
          'Authorization': globalAuthState.token!.token
        }),
      );
      return (json.decode(response.data) as List)
          .map((data) => VideoModel.fromJson(data))
          .toList();
    } catch (e) {
      if(e is DioError){
        throw ServerException(statusCode: e.response!.statusCode, stackTrace: e.stackTrace.toString());
      }else{
        throw CacheExceprion(stackTrace: (e as Error).stackTrace.toString());
      }
    }
  }

  @override
  Future<bool> createComment(int id, String comment, int videoId) async {
    try {
      final response = await Dio().post('$domain/videos/$id/comments',
          options: Options(headers: {
            'Content-Type': 'application/json',
            'Authorization': globalAuthState.token!.token
          }),
          data: jsonEncode({
            "user_id": globalAuthState.token!.id,
            "comment": comment,
            "name": globalAuthState.token!.name,
            "video_id": id
          }));
      return jsonDecode(response.data) as bool;
    } catch (e) {
      if(e is DioError){
        throw ServerException(statusCode: e.response!.statusCode, stackTrace: e.stackTrace.toString());
      }else{
        throw CacheExceprion(stackTrace: (e as Error).stackTrace.toString());
      }
    }
  }

  @override
  Future<List<CommentModel>> getComments(int id) async {
    try {
      final response = await Dio().get(
        '$domain/videos/$id/comments',
        options: Options(headers: {
          'Content-Type': 'application/json',
          'Authorization': globalAuthState.token!.token
        }),
      );

      return (json.decode(response.data) as List).map((model) {
        CommentModel data = CommentModel.fromJson(model);
        return data;
      }).toList();
    } catch (e) {
      if(e is DioError){
        throw ServerException(statusCode: e.response!.statusCode, stackTrace: e.stackTrace.toString());
      }else{
        throw CacheExceprion(stackTrace: (e as Error).stackTrace.toString());
      }
    }
  }

  @override
  Future<bool> createVideo(String name, String description, String path) async {
    try {
      final response = await Dio().post('$domain/videos',
          options: Options(headers: {
            'Content-Type': 'application/json',
            'Authorization': globalAuthState.token!.token
          }),
          data: jsonEncode({
            "name": name,
            "user_id": globalAuthState.token!.id,
            "path": path,
            "description": description
          }));
      return json.decode(response.data);
    } catch (e) {
      if(e is DioError){
        throw ServerException(statusCode: e.response!.statusCode, stackTrace: e.stackTrace.toString());
      }else{
        throw CacheExceprion(stackTrace: (e as Error).stackTrace.toString());
      }
    }
  }

  @override
  Future<List<VideoModel>> getUserVideos() async {
    try {
      final response = await Dio().get(
        '$domain/videos/users/${globalAuthState.token!.id}',
        options: Options(headers: {
          'Content-Type': 'application/json',
          'Authorization': globalAuthState.token!.token
        }),
      );
      return (json.decode(response.data) as List)
          .map((data) => VideoModel.fromJson(data))
          .toList();
    } catch (e) {
      if(e is DioError){
        throw ServerException(statusCode: e.response!.statusCode, stackTrace: e.stackTrace.toString());
      }else{
        throw CacheExceprion(stackTrace: (e as Error).stackTrace.toString());
      }
    }
  }

  @override
  Future<List<VideoModel>> getRelatedVideos(int id) async {
    try {
      final response = await Dio().get('$domain/related_videos/$id',
          options: Options(headers: {
            'Content-Type': 'application/json',
            'Authorization': globalAuthState.token!.token
          }),
          queryParameters: {"user_id": globalAuthState.token!.id});
      return (json.decode(response.data) as List)
          .map((data) => VideoModel.fromJson(data))
          .toList();
    } catch (e) {
      if(e is DioError){
        throw ServerException(statusCode: e.response!.statusCode, stackTrace: e.stackTrace.toString());
      }else{
        throw CacheExceprion(stackTrace: (e as Error).stackTrace.toString());
      }
    }
  }

  @override
  Future<bool> deleteComment(int commentId) async {
    try {
      final response = await Dio().delete('$domain/comments/$commentId',
          options: Options(headers: {
            'Content-Type': 'application/json',
            'Authorization': globalAuthState.token!.token
          }),
          data: {"user_id": globalAuthState.token!.id});

      return json.decode(response.data);
    } catch (e) {
      if(e is DioError){
        throw ServerException(statusCode: e.response!.statusCode, stackTrace: e.stackTrace.toString());
      }else{
        throw CacheExceprion(stackTrace: (e as Error).stackTrace.toString());
      }
    }
  }

  @override
  Future<bool> deleteVideo(int videoId) async {
    try {
      final response = await Dio().delete('$domain/videos/$videoId',
          options: Options(headers: {
            'Content-Type': 'application/json',
            'Authorization': globalAuthState.token!.token
          }),
          data: {"user_id": globalAuthState.token!.id});

      return json.decode(response.data);
    } catch (e) {
      if(e is DioError){
        throw ServerException(statusCode: e.response!.statusCode, stackTrace: e.stackTrace.toString());
      }else{
        throw CacheExceprion(stackTrace: (e as Error).stackTrace.toString());
      }
    }
  }

  @override
  Future<List<CommentModel>> updateComment(
      int id, String comment, int videoId) async {
    try {
      final response = await Dio().put('$domain/comments/$id',
          options: Options(headers: {
            'Content-Type': 'application/json',
            'Authorization': globalAuthState.token!.token
          }),
          data: {
            "user_id": globalAuthState.token!.id,
            "comment": comment,
            "name": globalAuthState.token!.name,
            "video_id": videoId
          });

      return (json.decode(response.data) as List).map((model) {
        CommentModel data = CommentModel.fromJson(model);
        return data;
      }).toList();
    } catch (e) {
      if(e is DioError){
        throw ServerException(statusCode: e.response!.statusCode, stackTrace: e.stackTrace.toString());
      }else{
        throw CacheExceprion(stackTrace: (e as Error).stackTrace.toString());
      }
    }
  }
  
  @override
  Future<List<VideoModel>> updateVideo(int id, String name, String description) async{
    try{
      final response = await Dio().put(
        '$domain/videos/$id',
        options: Options(headers: {
          'Content-Type': 'application/json',
          'Authorization': globalAuthState.token!.token
        }),
        data: {
          "user_id": globalAuthState.token!.id,
          "description": description,
          "name": name,
          "path": ""
        }
      );
      return (json.decode(response.data) as List)
          .map((data) => VideoModel.fromJson(data))
          .toList();
    } catch (e) {
      if(e is DioError){
        throw ServerException(statusCode: e.response!.statusCode, stackTrace: e.stackTrace.toString());
      }else{
        throw CacheExceprion(stackTrace: (e as Error).stackTrace.toString());
      }
    }
  }
  
  @override
  Future<VideoModel> getVideo(int id) async{
    try{
      final response = await Dio().get(
        '$domain/videos/$id',
        options: Options(headers: {
          'Content-Type': 'application/json',
          'Authorization': globalAuthState.token!.token
        }),
      );
      return VideoModel.fromJson(json.decode(response.data));
    } catch (e) {
      if(e is DioError){
        throw ServerException(statusCode: e.response!.statusCode, stackTrace: e.stackTrace.toString());
      }else{
        throw CacheExceprion(stackTrace: (e as Error).stackTrace.toString());
      }
    }
  }
}
