import 'package:bloc_example/data/video/repositories/video_repository.dart';
import 'package:bloc_example/model/comment_model.dart';
import 'package:bloc_example/model/comment_params.dart';
import 'package:bloc_example/model/errors.dart';
import 'package:bloc_example/model/use_case.dart';
import 'package:dartz/dartz.dart';

class UpdateComment implements UseCase<List<CommentModel>, CommentParams> {
  final IVideoRepository videoRepository;

  UpdateComment({required this.videoRepository});

  @override
  Future<Either<Failure, List<CommentModel>>> call(params) async{
    return await videoRepository.updateComment(params!.id, params.comment, params.videoId);
  }

}