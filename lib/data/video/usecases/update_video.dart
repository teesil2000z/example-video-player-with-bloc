import 'package:bloc_example/data/video/repositories/video_repository.dart';
import 'package:bloc_example/model/errors.dart';
import 'package:bloc_example/model/update_video_params.dart';
import 'package:bloc_example/model/use_case.dart';
import 'package:bloc_example/model/video_model.dart';
import 'package:dartz/dartz.dart';

class UpdateVideo implements UseCase<List<VideoModel>, UpdateVideoParams> {
  final IVideoRepository videoRepository;

  UpdateVideo({required this.videoRepository});

  @override
  Future<Either<Failure, List<VideoModel>>> call(params) async{
    return await videoRepository.updateVideo(params!.id, params.name, params.description);
  }

}