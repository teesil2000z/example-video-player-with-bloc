import 'package:bloc_example/data/video/repositories/video_repository.dart';
import 'package:bloc_example/model/errors.dart';
import 'package:bloc_example/model/use_case.dart';
import 'package:dartz/dartz.dart';

class DeleteVideo implements UseCase<bool, int> {
  final IVideoRepository videoRepository;

  DeleteVideo({required this.videoRepository});

  @override
  Future<Either<Failure, bool>> call(params) async{
    return await videoRepository.deleteVideo(params!);
  }

}