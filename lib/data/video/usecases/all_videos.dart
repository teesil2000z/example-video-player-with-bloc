
import 'package:bloc_example/data/video/repositories/video_repository.dart';
import 'package:bloc_example/model/errors.dart';
import 'package:bloc_example/model/use_case.dart';
import 'package:bloc_example/model/video_model.dart';
import 'package:dartz/dartz.dart';

class AllVideos implements UseCase<List<VideoModel>, dynamic> {
  final IVideoRepository videoRepository;

  AllVideos({required this.videoRepository});

  @override
  Future<Either<Failure, List<VideoModel>>> call(params) async{
    return await videoRepository.allVideos();
  }

}