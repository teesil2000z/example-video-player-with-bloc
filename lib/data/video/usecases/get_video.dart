import 'package:bloc_example/data/video/repositories/video_repository.dart';
import 'package:bloc_example/model/errors.dart';
import 'package:bloc_example/model/use_case.dart';
import 'package:bloc_example/model/video_model.dart';
import 'package:dartz/dartz.dart';

class GetVideo implements UseCase<VideoModel, int> {
  final IVideoRepository videoRepository;

  GetVideo({required this.videoRepository});

  @override
  Future<Either<Failure, VideoModel>> call(params) async{
    return await videoRepository.getVideo(params!);
  }

}