import 'package:bloc_example/data/video/repositories/video_repository.dart';
import 'package:bloc_example/model/comment_model.dart';
import 'package:bloc_example/model/errors.dart';
import 'package:bloc_example/model/use_case.dart';
import 'package:dartz/dartz.dart';

class GetComment implements UseCase<List<CommentModel>, int> {
  final IVideoRepository videoRepository;

  GetComment({required this.videoRepository});

  @override
  Future<Either<Failure, List<CommentModel>>> call(params) async{
    return await videoRepository.getComments(params!);
  }

}