import 'package:bloc_example/data/video/repositories/video_repository.dart';
import 'package:bloc_example/model/errors.dart';
import 'package:bloc_example/model/use_case.dart';
import 'package:bloc_example/model/video_model.dart';
import 'package:dartz/dartz.dart';

class GetUserVideos implements UseCase<List<VideoModel>, dynamic> {
  final IVideoRepository videoRepository;

  GetUserVideos({required this.videoRepository});

  @override
  Future<Either<Failure, List<VideoModel>>> call(params) async{
    return await videoRepository.getUserVideos();
  }

}