
import 'package:bloc_example/data/video/repositories/video_repository.dart';
import 'package:bloc_example/model/comment_params.dart';
import 'package:bloc_example/model/errors.dart';
import 'package:bloc_example/model/use_case.dart';
import 'package:dartz/dartz.dart';

class CreateComment implements UseCase<bool, CommentParams> {
  final IVideoRepository videoRepository;

  CreateComment({required this.videoRepository});

  @override
  Future<Either<Failure, bool>> call(params) async{
    return await videoRepository.createComment(params!.id, params.comment, params.videoId);
  }

}