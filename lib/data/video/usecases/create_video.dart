import 'package:bloc_example/data/video/repositories/video_repository.dart';
import 'package:bloc_example/model/errors.dart';
import 'package:bloc_example/model/use_case.dart';
import 'package:bloc_example/model/video_params.dart';
import 'package:dartz/dartz.dart';

class CreateVideo implements UseCase<bool, VideoParams> {
  final IVideoRepository videoRepository;

  CreateVideo({required this.videoRepository});

  @override
  Future<Either<Failure, bool>> call(params) async{
    return await videoRepository.createVideo(params!.name, params.description, params.path);
  }

}