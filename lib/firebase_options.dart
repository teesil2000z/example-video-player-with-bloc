// File generated by FlutterFire CLI.
// ignore_for_file: lines_longer_than_80_chars, avoid_classes_with_only_static_members
import 'package:firebase_core/firebase_core.dart' show FirebaseOptions;
import 'package:flutter/foundation.dart'
    show defaultTargetPlatform, kIsWeb, TargetPlatform;

/// Default [FirebaseOptions] for use with your Firebase apps.
///
/// Example:
/// ```dart
/// import 'firebase_options.dart';
/// // ...
/// await Firebase.initializeApp(
///   options: DefaultFirebaseOptions.currentPlatform,
/// );
/// ```
class DefaultFirebaseOptions {
  static FirebaseOptions get currentPlatform {
    if (kIsWeb) {
      return web;
    }
    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        return android;
      case TargetPlatform.iOS:
        return ios;
      case TargetPlatform.macOS:
        return macos;
      case TargetPlatform.windows:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for windows - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      case TargetPlatform.linux:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for linux - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      default:
        throw UnsupportedError(
          'DefaultFirebaseOptions are not supported for this platform.',
        );
    }
  }

  static const FirebaseOptions web = FirebaseOptions(
    apiKey: 'AIzaSyCQJWAWt_5iQ320T0qe-R-4k2lsZop5_9A',
    appId: '1:306952387275:web:86708bb58a1c4ce838794e',
    messagingSenderId: '306952387275',
    projectId: 'test-task-67bb2',
    authDomain: 'test-task-67bb2.firebaseapp.com',
    storageBucket: 'test-task-67bb2.appspot.com',
  );

  static const FirebaseOptions android = FirebaseOptions(
    apiKey: 'AIzaSyBmy_Q_vH6XgIq6i2YUgp96vmeMg2qMLjQ',
    appId: '1:306952387275:android:cc0abc845361f0f038794e',
    messagingSenderId: '306952387275',
    projectId: 'test-task-67bb2',
    storageBucket: 'test-task-67bb2.appspot.com',
  );

  static const FirebaseOptions ios = FirebaseOptions(
    apiKey: 'AIzaSyB8KFzmvAqgcJyDoA8X2sBFW1dRJ7MZllo',
    appId: '1:306952387275:ios:96d705c43febf91438794e',
    messagingSenderId: '306952387275',
    projectId: 'test-task-67bb2',
    storageBucket: 'test-task-67bb2.appspot.com',
    iosClientId: '306952387275-40tld99esesicdacvg9f3d1jkhb5nrli.apps.googleusercontent.com',
    iosBundleId: 'com.example.blocExample',
  );

  static const FirebaseOptions macos = FirebaseOptions(
    apiKey: 'AIzaSyB8KFzmvAqgcJyDoA8X2sBFW1dRJ7MZllo',
    appId: '1:306952387275:ios:96d705c43febf91438794e',
    messagingSenderId: '306952387275',
    projectId: 'test-task-67bb2',
    storageBucket: 'test-task-67bb2.appspot.com',
    iosClientId: '306952387275-40tld99esesicdacvg9f3d1jkhb5nrli.apps.googleusercontent.com',
    iosBundleId: 'com.example.blocExample',
  );
}
