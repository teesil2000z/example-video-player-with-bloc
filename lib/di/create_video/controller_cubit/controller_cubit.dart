import 'package:bloc/bloc.dart';

enum VideoControllerState{empty, loaded}
class VideoControllerCubit extends Cubit<VideoControllerState>{
  VideoControllerCubit() : super(VideoControllerState.empty);

  void changePlayerState(VideoControllerState selection) => emit(selection);
}