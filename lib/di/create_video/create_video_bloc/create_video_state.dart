part of 'create_video_cubit.dart';

enum CreateVideoAct {loading, created, init, error}

class CreateVideoState extends Equatable{
  final String title;
  final String descriprion;
  final CreateVideoAct createVideoAct;
  
  const CreateVideoState({
    required this.title,
    required this.descriprion,
    required this.createVideoAct
  });

  @override
  List<Object?> get props => [title, descriprion, createVideoAct];

  CreateVideoState copyWith({
    String? title,
    String? descriprion,
    CreateVideoAct? createVideoAct
  }){
    return CreateVideoState(
      title: title ?? this.title, 
      descriprion: descriprion ?? this.descriprion, 
      createVideoAct: createVideoAct ?? this.createVideoAct
    );
  }

}