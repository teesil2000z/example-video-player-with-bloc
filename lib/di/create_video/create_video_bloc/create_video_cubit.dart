import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:bloc_example/data/video/usecases/create_video.dart';
import 'package:bloc_example/model/video_params.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:equatable/equatable.dart';

part 'create_video_state.dart';

class CreateVideoCubit extends Cubit<CreateVideoState>{
  final CreateVideo createVideo;
  CreateVideoCubit({required this.createVideo}) : super(const CreateVideoState(
    title: '', 
    descriprion: '', 
    createVideoAct: CreateVideoAct.init,
  ));

  void createVideoAct({required File video}) async{
    if(state.title.isNotEmpty && state.descriprion.isNotEmpty){
      emit(state.copyWith(createVideoAct: CreateVideoAct.loading));
      final firebaseStorage = FirebaseStorage.instance;
      bool firebaseFlag = true;
      var snapshot = await firebaseStorage.ref()
        .child(video.path.split('/').last)
        .putFile(video).onError((error, stackTrace) {
          firebaseFlag = false;
          emit(state.copyWith(createVideoAct: CreateVideoAct.error));
          throw Exception(error);
        }
      );
      var url = await snapshot.ref.getDownloadURL();
      final failureOrUploaded = await createVideo.call(VideoParams(
        name: state.title, 
        description: state.descriprion, 
        path: url
      ));
      CreateVideoState newState = failureOrUploaded.fold(
        (l) => state.copyWith(createVideoAct: CreateVideoAct.error), 
        (r) => state.copyWith(createVideoAct: CreateVideoAct.created)
      );
      if(firebaseFlag){
        emit(newState);
      }
    }
  }

  void toInit(){
    emit(state.copyWith(
      createVideoAct: CreateVideoAct.init,
      title: '',
      descriprion: '',
    ));
  }

  void setTitle(String title){
    emit(state.copyWith(title: title));
  }

  void setDescription(String descriprion){
    emit(state.copyWith(descriprion: descriprion));
  }
}