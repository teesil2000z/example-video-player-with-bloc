import 'package:bloc/bloc.dart';

enum NavigationSelection{flow, user}
class NavigationCubit extends Cubit<NavigationSelection>{
  NavigationCubit() : super(NavigationSelection.flow);

  void changeSelection(NavigationSelection selection) => emit(selection);

}