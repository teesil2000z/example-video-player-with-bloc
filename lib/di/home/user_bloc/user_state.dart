

import 'package:bloc_example/model/user_data.dart';
import 'package:bloc_example/model/video_model.dart';
import 'package:equatable/equatable.dart';

interface class UserState extends Equatable {
  @override
  List<Object?> get props => [];
}

class UserInitState extends UserState {
  @override
  List<Object?> get props => [];
}

class UserLoadingState extends UserState {
  @override
  List<Object?> get props => [];
}

class UserLoadedState extends UserState {

  final UserData userData;
  final List<VideoModel> videos;

  UserLoadedState({
    required this.userData,
    required this.videos
  });

  @override
  List<Object?> get props => [userData, videos];
}

class UserErrorState extends UserState {
  @override
  List<Object?> get props => [];
}