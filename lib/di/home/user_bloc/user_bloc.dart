import 'package:bloc/bloc.dart';
import 'package:bloc_example/data/auth/usecases/get_user.dart';
import 'package:bloc_example/data/video/usecases/delete_video.dart';
import 'package:bloc_example/data/video/usecases/get_user_videos.dart';
import 'package:bloc_example/data/video/usecases/update_video.dart';
import 'package:bloc_example/di/home/user_bloc/user_event.dart';
import 'package:bloc_example/di/home/user_bloc/user_state.dart';
import 'package:bloc_example/model/update_video_params.dart';

class UserBloc extends Bloc<UserEvent, UserState>{
  final GetUser getUser;
  final GetUserVideos getUserVideos;
  final DeleteVideo deleteVideo;
  final UpdateVideo updateVideo;
  UserBloc({
    required this.updateVideo,
    required this.getUser,
    required this.getUserVideos,
    required this.deleteVideo
  }) : super(UserInitState()){
    on<LoadUserEvent>((event, emit) async{
      emit(UserLoadingState());
      final failuteOrUser = await getUser(null);
      UserState newState = await failuteOrUser.fold(
        (l) => UserErrorState(), 
        (user) async {
          final failuteOrVideos = await getUserVideos(null);
          UserState states = await failuteOrVideos.fold(
            (l) => UserErrorState(), 
            (videos) => UserLoadedState(userData: user, videos: videos)
          );
          return states;
        }
      );
      emit(newState);
    });
    on<DeleteVideoEvent>((event, emit) async{
      emit(UserLoadingState());
      final failuteOrUser = await deleteVideo(event.videoId);
      UserState newState = await failuteOrUser.fold(
        (l) => UserErrorState(), 
        (user) async {
          final failuteOrVideos = await getUserVideos(null);
          UserState states = await failuteOrVideos.fold(
            (l) => UserErrorState(), 
            (videos) => UserLoadedState(userData: event.userData, videos: videos)
          );
          return states;
        }
      );
      emit(newState);
    });
    on<UpdateVideoEvent>((event, emit) async{
      emit(UserLoadingState());
      final failuteOrUser = await updateVideo(UpdateVideoParams(
        description: event.description, 
        id: event.id, 
        name: event.name
      ));
      UserState newState = await failuteOrUser.fold(
        (l) => UserErrorState(), 
        (videos) => UserLoadedState(userData: event.userData, videos: videos)
      );
      emit(newState);
      add(LoadUserEvent());
    });
  }
}