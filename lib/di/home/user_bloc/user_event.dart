import 'package:bloc_example/model/user_data.dart';

interface class UserEvent {}

class LoadUserEvent extends UserEvent {}

class DeleteVideoEvent extends UserEvent {
  final UserData userData;
  final int videoId;
  DeleteVideoEvent({
    required this.userData,
    required this.videoId
  });
}
class UpdateVideoEvent extends UserEvent {
  final UserData userData;
  final String name;
  final int id;
  final String description;

  UpdateVideoEvent(
    {
      required this.description,
      required this.id,
      required this.name,
      required this.userData
    }
  );

}