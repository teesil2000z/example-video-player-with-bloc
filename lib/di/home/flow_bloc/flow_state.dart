

import 'package:bloc_example/model/video_model.dart';
import 'package:equatable/equatable.dart';

interface class FlowState extends Equatable{
  @override
  List<Object?> get props => [];
}

class FlowLoadingState extends FlowState{
  @override
  List<Object?> get props => [];
}

class FlowLoadedState extends FlowState{
  final List<VideoModel> data;

  FlowLoadedState({required this.data});
  @override
  List<Object?> get props => [data];
}

class FlowInitState extends FlowState {
  @override
  List<Object?> get props => [];
}

class FlowErrorState extends FlowState {
  @override
  List<Object?> get props => [];
}
