import 'package:bloc/bloc.dart';
import 'package:bloc_example/data/video/usecases/all_videos.dart';
import 'package:bloc_example/di/home/flow_bloc/flow_event.dart';
import 'package:bloc_example/di/home/flow_bloc/flow_state.dart';

class FlowBloc extends Bloc<FlowEvent, FlowState>{
  final AllVideos allVideos;
  FlowBloc({required this.allVideos}) : super(FlowInitState()){
    on<FlowLoadEvent>((event, emit) async {
      emit(FlowLoadingState());
      final failuteOrVideos = await allVideos(null);
      FlowState newState = failuteOrVideos.fold(
        (l) => FlowErrorState(), 
        (r) => FlowLoadedState(data: r)
      );
      emit(newState);
    });
  }

}