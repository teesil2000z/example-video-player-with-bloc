part of 'comments_cubit.dart';

interface class CommentsState extends Equatable{
  final String comment;

  const CommentsState({required this.comment});

  @override
  List<Object?> get props => [comment];

  CommentsState copyWith({String? comment}){
    return CommentsState(
      comment: comment ?? this.comment
    );
  }
}

class CommentsInitState extends CommentsState{
  const CommentsInitState({required super.comment});

  @override
  List<Object?> get props => [comment];

  @override
  CommentsInitState copyWith({String? comment}){
    return CommentsInitState(
      comment: comment ?? this.comment
    );
  }
}

class CommentAddedState extends CommentsState{
  final List<CommentModel> comments;
  const CommentAddedState({required this.comments, required super.comment});
  
  @override
  List<Object?> get props => [comments, comment];

  @override
  CommentAddedState copyWith({String? comment, List<CommentModel>? comments}){
    return CommentAddedState(
      comment: comment ?? this.comment,
      comments: comments ?? this.comments
    );
  }
}

class CommentsLoadingState extends CommentsState {
  const CommentsLoadingState({required super.comment});

  @override
  List<Object?> get props => [comment];
}

class CommentsErrorState extends CommentsState {
  const CommentsErrorState({required super.comment});

  @override
  List<Object?> get props => [comment];

  @override
  CommentsErrorState copyWith({String? comment, List<CommentModel>? comments}){
    return CommentsErrorState(
      comment: comment ?? this.comment
    );
  }
}