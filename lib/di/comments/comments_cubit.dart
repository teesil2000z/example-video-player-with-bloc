import 'package:bloc/bloc.dart';
import 'package:bloc_example/data/video/usecases/create_comment.dart';
import 'package:bloc_example/data/video/usecases/delete_comment.dart';
import 'package:bloc_example/data/video/usecases/get_comments.dart';
import 'package:bloc_example/data/video/usecases/update_comment.dart';
import 'package:bloc_example/model/comment_params.dart';
import 'package:bloc_example/model/comment_model.dart';
import 'package:equatable/equatable.dart';

part 'comments_state.dart';

class CommentsCubit extends Cubit<CommentsState> {
  final CreateComment createComment;
  final GetComment getComment;
  final DeleteComment deleteComment;
  final UpdateComment updateComment;
  CommentsCubit({required this.createComment, required this.getComment, required this.deleteComment, required this.updateComment})
      : super(const CommentsInitState(comment: ''));

    void setComment(String comment){
      emit(state.copyWith(comment: comment));
    }

    void addComment({required int id}) async{
      if(state.comment.isNotEmpty){
        emit(CommentsLoadingState(comment: state.comment));
        final failuteOrC = await createComment(
            CommentParams(id: id, comment: state.comment, videoId: id));
        CommentsState newState = await failuteOrC.fold((l) async {
          return CommentsErrorState(comment: state.comment);
        }, (r) async {
          final failuteOrComments = await getComment(id);
          CommentsState newState = failuteOrComments.fold(
              (l) => CommentsErrorState(comment: state.comment), (success) => CommentAddedState(comments: success, comment: state.comment));
          return newState;
        });
        emit(newState);
      }
    }

    void loadComments(int id) async{
      emit(CommentsLoadingState(comment: state.comment));
      final failuteOrComments = await getComment(id);
      CommentsState newState = failuteOrComments.fold(
          (l) => CommentsErrorState(comment: state.comment), (r) => CommentAddedState(comments: r, comment: state.comment));
      emit(newState);
    }

    void deleteCommentEv({required int commentId, required int id}) async{
      emit(CommentsLoadingState(comment: state.comment));
      final failuteOrC = await deleteComment(commentId);
      CommentsState newState = await failuteOrC.fold((l) async {
        return CommentsErrorState(comment: state.comment);
      }, (r) async {
        final failuteOrComments = await getComment(id);
        CommentsState newState = failuteOrComments.fold(
            (l) => CommentsErrorState(comment: state.comment), (success) => CommentAddedState(comments: success, comment: state.comment));
        return newState;
      });
      emit(newState);
    }

    void updateCommentEv({required String comment, required int id, required int videoId}) async{
      emit(CommentsLoadingState(comment: state.comment));
      final failuteOrComments = await updateComment(CommentParams(
        comment: comment, 
        id: id, 
        videoId: videoId
      ));
      CommentsState newState = failuteOrComments.fold(
          (l) => CommentsErrorState(comment: state.comment), (r) => CommentAddedState(comments: r, comment: state.comment));
      emit(newState);
    }
  
}
