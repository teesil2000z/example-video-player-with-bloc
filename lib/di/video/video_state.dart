

import 'package:bloc_example/model/video_model.dart';
import 'package:equatable/equatable.dart';

interface class VideoState extends Equatable{
  @override
  List<Object?> get props => [];
}

class VideoLoadingState extends VideoState{
  @override
  List<Object?> get props => [];
}

class VideoLoadedState extends VideoState{
  final List<VideoModel> data;
  final VideoModel model;

  VideoLoadedState({required this.data, required this.model});
  
  @override
  List<Object?> get props => [data, model];
}

class VideoInitState extends VideoState {
  @override
  List<Object?> get props => [];
}

class VideoErrorState extends VideoState {
  @override
  List<Object?> get props => [];
}
