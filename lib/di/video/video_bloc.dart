import 'package:bloc/bloc.dart';
import 'package:bloc_example/data/video/usecases/get_related_videos.dart';
import 'package:bloc_example/data/video/usecases/get_video.dart';
import 'package:bloc_example/di/video/video_event.dart';
import 'package:bloc_example/di/video/video_state.dart';
import 'package:bloc_example/model/related_videos_params.dart';

class VideoBloc extends Bloc<VideoEvent, VideoState>{
  final GetRelatedVideos allVideos;
  final GetVideo getVideo;
  VideoBloc({required this.allVideos, required this.getVideo}) : super(VideoInitState()){
    on<VideoLoadEvent>((event, emit) async {
      emit(VideoLoadingState());
      await (await getVideo(event.id)).fold(
        (l) {
          emit(VideoErrorState());
        }, 
        (video) async{
          (await allVideos(RelatedVideosParams(id: event.id))).fold(
            (l) {emit(VideoErrorState());}, 
            (r) {emit(VideoLoadedState(data: r, model: video));}
          );
        } 
      );
    });
  }

}