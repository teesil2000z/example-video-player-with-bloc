part of 'login_cubit.dart';

enum LoginBechaviorEnum {error, none, authorized, logout, loading}
class LoginState extends Equatable{

  final LoginBechaviorEnum loginBechaviorEnum;
  final String email;
  final String password;
  final String errorMessage;

  const LoginState({
    required this.email, 
    required this.password, 
    this.loginBechaviorEnum = LoginBechaviorEnum.none, 
    required this.errorMessage
  });

  @override
  List<Object?> get props => [email, password, loginBechaviorEnum];

  LoginState copyWith({
    final String? email,
    final String? password,
    final LoginBechaviorEnum? loginBechaviorEnum,
    final String? errorMessage
  }){
    return LoginState(
      errorMessage: errorMessage ?? this.errorMessage,
      email: email ?? this.email, 
      password: password ?? this.password, 
      loginBechaviorEnum: loginBechaviorEnum ?? this.loginBechaviorEnum
    );
  }
}