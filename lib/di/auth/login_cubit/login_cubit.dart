import 'package:bloc/bloc.dart';
import 'package:bloc_example/data/auth/usecases/auth.dart';
import 'package:bloc_example/data/auth/usecases/logout.dart';
import 'package:bloc_example/model/auth_params.dart';
import 'package:bloc_example/model/global_auth_state.dart';
import 'package:bloc_example/model/token_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:equatable/equatable.dart';

part 'login_state.dart';

class LoginCubit extends Cubit<LoginState> {
  final Auth auth;
  final Logout logout;
  final GlobalAuthState globalAuthState;

  LoginCubit({required this.auth, required this.globalAuthState, required this.logout}) : super(const LoginState(
    email: '',
    password: '',
    loginBechaviorEnum: LoginBechaviorEnum.none,
    errorMessage: ''
  )); 

  void setEmail(String email){
    emit(state.copyWith(email: email));
  }

  void setPassword(String password){
    emit(state.copyWith(password: password));
  }

  void login() async {
    emit(state.copyWith(loginBechaviorEnum: LoginBechaviorEnum.loading));
    final failuteOrLogin = await auth(AuthParams(email: state.email, password: state.password));
    LoginState newState = failuteOrLogin.fold(
      (l) => state.copyWith(
        loginBechaviorEnum: LoginBechaviorEnum.error, 
        errorMessage: 'incorrect email or password'
      ), 
      (r) => state.copyWith(loginBechaviorEnum: LoginBechaviorEnum.authorized)
    );
    emit(newState);
  }

  void checkToken() async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    final tokenPref = sharedPreferences.getString("TOKEN") ?? '';
    final namePref = sharedPreferences.getString("NAME") ?? '';
    final idPref = sharedPreferences.getInt("ID") ?? 0;
    final emailPref = sharedPreferences.getString("EMAIL") ?? '';
    TokenModel model = TokenModel(
      email: emailPref,
      token: tokenPref, 
      id: idPref, 
      name: namePref
    );
    if(model.token!=''){
      globalAuthState.token = model;
      emit(state.copyWith(loginBechaviorEnum: LoginBechaviorEnum.authorized));
    }else{
      emit(state.copyWith(loginBechaviorEnum: LoginBechaviorEnum.logout));
    }
  }

  void logOut(){
    logout.call(null);
    emit(state.copyWith(loginBechaviorEnum: LoginBechaviorEnum.logout));
  }

  void toInit(){
    emit(state.copyWith(loginBechaviorEnum: LoginBechaviorEnum.none));
  }
  
}
