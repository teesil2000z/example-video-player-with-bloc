import 'package:bloc/bloc.dart';
import 'package:bloc_example/data/auth/usecases/sign_up.dart';
import 'package:bloc_example/model/sign_up_params.dart';
import 'package:equatable/equatable.dart';

part 'sign_up_state.dart';

class SignUpCubit extends Cubit<SignUpState> {
  final SignUp signUp;
  SignUpCubit({required this.signUp}) : super(const SignUpState(
    email: '', 
    errorMessage: '', 
    name: '', 
    password: '',
    signUpBechaviorEnum: SignUpBechaviorEnum.none
  )); 

  void setEmail(String email){
    emit(state.copyWith(email: email));
  }

  void setPassword(String password){
    emit(state.copyWith(password: password));
  }

  void setName(String name){
    emit(state.copyWith(name: name));
  }

  void toInit(){
    emit(state.copyWith(signUpBechaviorEnum: SignUpBechaviorEnum.none));
  }

  void performSignUp() async{
    emit(state.copyWith(signUpBechaviorEnum: SignUpBechaviorEnum.loading));
    final failuteOrVideos = await signUp(SignUpParams(email: state.email, password: state.password, name: state.name));
    SignUpState newState = failuteOrVideos.fold(
      (l) => state.copyWith(signUpBechaviorEnum: SignUpBechaviorEnum.loading, errorMessage: 'incorrect email or password'), 
      (r) => state.copyWith(signUpBechaviorEnum: SignUpBechaviorEnum.signedUp)
    );
    emit(newState);
  }

}
