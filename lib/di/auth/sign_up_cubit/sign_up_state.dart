part of 'sign_up_cubit.dart';

enum SignUpBechaviorEnum{none, loading, signedUp, error}
class SignUpState extends Equatable {
  final String email;
  final String password;
  final String name;
  final SignUpBechaviorEnum signUpBechaviorEnum;
  final String errorMessage;

  const SignUpState({
    required this.email,
    required this.errorMessage,
    required this.name,
    required this.password,
    this.signUpBechaviorEnum = SignUpBechaviorEnum.none
  });

  @override
  List<Object?> get props => [email, password, name, signUpBechaviorEnum, errorMessage];

  SignUpState copyWith({
    final String? email,
    final String? password,
    final String? name,
    final String? errorMessage,
    final SignUpBechaviorEnum? signUpBechaviorEnum
  }){
    return SignUpState(
      email: email ?? this.email, 
      errorMessage: errorMessage ?? this.errorMessage, 
      name: name ?? this.name, 
      password: password ?? this.password,
      signUpBechaviorEnum: signUpBechaviorEnum ?? this.signUpBechaviorEnum
    );
  }
}