import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
part 'related_videos_params.g.dart';

@immutable
@JsonSerializable()
class RelatedVideosParams{
  @JsonKey(name: "id")
  final int id;

  const RelatedVideosParams({required this.id});

  factory RelatedVideosParams.fromJson(Map<String, dynamic> json) => _$RelatedVideosParamsFromJson(json);
  Map<String, dynamic> toJson() => _$RelatedVideosParamsToJson(this);
}