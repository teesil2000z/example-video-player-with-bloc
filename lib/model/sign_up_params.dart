import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
part 'sign_up_params.g.dart';

@immutable
@JsonSerializable()
class SignUpParams{
  @JsonKey(name: "name")
  final String name;
  @JsonKey(name: "email")
  final String email;
  @JsonKey(name: "password")
  final String password;
  
  const SignUpParams({required this.email, required this.password, required this.name});

  factory SignUpParams.fromJson(Map<String, dynamic> json) => _$SignUpParamsFromJson(json);
  Map<String, dynamic> toJson() => _$SignUpParamsToJson(this);
}