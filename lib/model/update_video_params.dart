class UpdateVideoParams{
  final int id;
  final String name;
  final String description;

  UpdateVideoParams({required this.description, required this.id, required this.name});
}