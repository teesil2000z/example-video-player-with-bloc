import 'package:equatable/equatable.dart';

class ServerException implements Exception {
  final int? statusCode;
  final String? stackTrace;
  ServerException({this.statusCode, required this.stackTrace});
}

class CacheExceprion implements Exception {
  final String? stackTrace;

  CacheExceprion({required this.stackTrace});
}

interface class Failure extends Equatable {
  const Failure();
  @override
  List<Object?> get props => [];
}

class ServerFailure extends Failure{
  final int? statusCode;

  const ServerFailure({this.statusCode});
}

class CacheFailure extends Failure {
  const CacheFailure();
}