// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'related_videos_params.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RelatedVideosParams _$RelatedVideosParamsFromJson(Map<String, dynamic> json) =>
    RelatedVideosParams(
      id: json['id'] as int,
    );

Map<String, dynamic> _$RelatedVideosParamsToJson(
        RelatedVideosParams instance) =>
    <String, dynamic>{
      'id': instance.id,
    };
