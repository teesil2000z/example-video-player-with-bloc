
import 'package:bloc_example/model/errors.dart';
import 'package:dartz/dartz.dart';

abstract interface class UseCase<Type, Params> {
  Future<Either<Failure, Type>> call(Params? params);
}