import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
part 'video_model.g.dart';

VideoModel deserializeVideoModelList(Map<String, dynamic> json) => 
    VideoModel.fromJson(json);

Map<String, dynamic> serializeVideoModelList(VideoModel object) => object.toJson();

@immutable
@JsonSerializable()
class VideoModel extends Equatable{
  const VideoModel(
      {required this.id,
      required this.name,
      required this.path,
      required this.description,
      required this.userId});
  @JsonKey(name: "id")
  final int id;
  @JsonKey(name: "name")
  final String name;
  @JsonKey(name: "path")
  final String path;
  @JsonKey(name: "description")
  final String description;
  @JsonKey(name: "user_id")
  final int userId;

  factory VideoModel.fromJson(Map<String, dynamic> json) =>
      _$VideoModelFromJson(json);
  Map<String, dynamic> toJson() => _$VideoModelToJson(this);
  
  @override
  List<Object?> get props => [id, name, path, description, userId];
}
