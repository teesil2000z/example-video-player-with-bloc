// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'comment_params.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CommentParams _$CommentParamsFromJson(Map<String, dynamic> json) =>
    CommentParams(
      comment: json['comment'] as String,
      id: json['id'] as int,
      videoId: json['video_id'] as int,
    );

Map<String, dynamic> _$CommentParamsToJson(CommentParams instance) =>
    <String, dynamic>{
      'comment': instance.comment,
      'id': instance.id,
      'video_id': instance.videoId,
    };
