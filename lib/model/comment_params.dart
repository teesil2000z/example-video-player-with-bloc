import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
part 'comment_params.g.dart';

@immutable
@JsonSerializable()
class CommentParams{
  @JsonKey(name: "comment")
  final String comment;
  @JsonKey(name: "id")
  final int id;
  @JsonKey(name: "video_id")
  final int videoId;
  
  const CommentParams({required this.comment, required this.id, required this.videoId});

  factory CommentParams.fromJson(Map<String, dynamic> json) => _$CommentParamsFromJson(json);
  Map<String, dynamic> toJson() => _$CommentParamsToJson(this);
}