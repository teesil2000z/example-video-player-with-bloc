// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth_params.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AuthParams _$AuthParamsFromJson(Map<String, dynamic> json) => AuthParams(
      email: json['email'] as String,
      password: json['password'] as String,
    );

Map<String, dynamic> _$AuthParamsToJson(AuthParams instance) =>
    <String, dynamic>{
      'email': instance.email,
      'password': instance.password,
    };
