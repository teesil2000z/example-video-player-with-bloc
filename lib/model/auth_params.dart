import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
part 'auth_params.g.dart';

@immutable
@JsonSerializable()
class AuthParams{
  @JsonKey(name: "email")
  final String email;
  @JsonKey(name: "password")
  final String password;
  
  const AuthParams({required this.email, required this.password});

  AuthParams copyWith({
    required final String email,
    required final String password
  }){
    return AuthParams(
      email: email, 
      password: password
    );
  }
  factory AuthParams.fromJson(Map<String, dynamic> json) => _$AuthParamsFromJson(json);
  Map<String, dynamic> toJson() => _$AuthParamsToJson(this);
}




