import 'package:bloc_example/model/global_auth_state.dart';
import 'package:equatable/equatable.dart';
import 'package:get_it/get_it.dart';
import 'package:json_annotation/json_annotation.dart';
part 'comment_model.g.dart';

@JsonSerializable()
class CommentModel extends Equatable{

  @JsonKey(name: "id")
  final int id;
  @JsonKey(name: "name")
  final String name;
  @JsonKey(name: "comment")
  final String comment;
  @JsonKey(name: "video_id")
  final int videoId;
  @JsonKey(name: "user_id")
  final int userId;
  
  bool get isMine => GetIt.I<GlobalAuthState>().token!=null ? GetIt.I<GlobalAuthState>().token!.id == userId : false;

  const CommentModel({
    required this.comment,
    required this.id,
    required this.name,
    required this.videoId,
    required this.userId
  });

  factory CommentModel.fromJson(Map<String, dynamic> json) => _$CommentModelFromJson(json);
  Map<String, dynamic> toJson() => _$CommentModelToJson(this);
  @override
  List<Object?> get props => [name, id, comment, videoId, userId];
}