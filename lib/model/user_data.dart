import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
part 'user_data.g.dart';

@immutable
@JsonSerializable()
class UserData extends Equatable {

  @JsonKey(name: "name")
  final String name;
  @JsonKey(name: "email")
  final String email;
  @JsonKey(name: "id")
  final int id;

  const UserData({
    required this.email,
    required this.name,
    required this.id
  });
  factory UserData.fromJson(Map<String, dynamic> json) => _$UserDataFromJson(json);
  Map<String, dynamic> toJson() => _$UserDataToJson(this);

  @override
  List<Object?> get props => [name, email, id];
}