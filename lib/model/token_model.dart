import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
part 'token_model.g.dart';

@immutable
@JsonSerializable()
class TokenModel{
  const TokenModel({
    required this.token,
    required this.id,
    required this.name,
    required this.email
  });
  @JsonKey(name: "token")
  final String token;
  @JsonKey(name: "name")
  final String name;
  @JsonKey(name: "id")
  final int id;
  @JsonKey(name: "email")
  final String email;

  factory TokenModel.fromJson(Map<String, dynamic> json) => _$TokenModelFromJson(json);
  Map<String, dynamic> toJson() => _$TokenModelToJson(this);
}