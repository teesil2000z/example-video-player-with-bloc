import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
part 'video_params.g.dart';

@immutable
@JsonSerializable()
class VideoParams {
  @JsonKey(name: "name")
  final String name;
  @JsonKey(name: "description")
  final String description;
  @JsonKey(name: "path")
  final String path;

  const VideoParams(
      {required this.name, required this.description, required this.path});

  factory VideoParams.fromJson(Map<String, dynamic> json) =>
      _$VideoParamsFromJson(json);
  Map<String, dynamic> toJson() => _$VideoParamsToJson(this);
}
