// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'video_params.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VideoParams _$VideoParamsFromJson(Map<String, dynamic> json) => VideoParams(
      name: json['name'] as String,
      description: json['description'] as String,
      path: json['path'] as String,
    );

Map<String, dynamic> _$VideoParamsToJson(VideoParams instance) =>
    <String, dynamic>{
      'name': instance.name,
      'description': instance.description,
      'path': instance.path,
    };
