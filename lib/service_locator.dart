import 'package:bloc_example/data/auth/datasourse/auth_local_data_source.dart';
import 'package:bloc_example/data/auth/datasourse/auth_remote_data_source.dart';
import 'package:bloc_example/data/auth/repositories/auth_repository.dart';
import 'package:bloc_example/data/auth/usecases/auth.dart';
import 'package:bloc_example/data/auth/usecases/get_user.dart';
import 'package:bloc_example/data/auth/usecases/logout.dart';
import 'package:bloc_example/data/auth/usecases/sign_up.dart';
import 'package:bloc_example/data/video/datasourse/video_remote_data_source.dart';
import 'package:bloc_example/data/video/repositories/video_repository.dart';
import 'package:bloc_example/data/video/usecases/all_videos.dart';
import 'package:bloc_example/data/video/usecases/create_comment.dart';
import 'package:bloc_example/data/video/usecases/create_video.dart';
import 'package:bloc_example/data/video/usecases/delete_comment.dart';
import 'package:bloc_example/data/video/usecases/delete_video.dart';
import 'package:bloc_example/data/video/usecases/get_comments.dart';
import 'package:bloc_example/data/video/usecases/get_related_videos.dart';
import 'package:bloc_example/data/video/usecases/get_user_videos.dart';
import 'package:bloc_example/data/video/usecases/get_video.dart';
import 'package:bloc_example/data/video/usecases/update_comment.dart';
import 'package:bloc_example/data/video/usecases/update_video.dart';
import 'package:bloc_example/di/auth/login_cubit/login_cubit.dart';
import 'package:bloc_example/di/home/flow_bloc/flow_bloc.dart';
import 'package:bloc_example/di/home/user_bloc/user_bloc.dart';
import 'package:bloc_example/model/global_auth_state.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';

final sl = GetIt.instance;

Future<void> init() async {
  sl.registerFactory(
    () => FlowBloc(
      allVideos: sl<AllVideos>()
    )
  );
  sl.registerFactory(
    () => LoginCubit(
      logout: sl<Logout>(),
      auth: sl<Auth>(),
      globalAuthState: sl<GlobalAuthState>()
    )
  );
  
  sl.registerFactory(
    () => UserBloc(
      updateVideo: sl<UpdateVideo>(),
      deleteVideo: sl<DeleteVideo>(),
      getUser: sl<GetUser>(), 
      getUserVideos: sl<GetUserVideos>()
    )
  );

  sl.registerLazySingleton<IVideoRemoteDataSource>(
    () => VideoRemoteDataSource(
      globalAuthState: sl()
    )
  );

  sl.registerLazySingleton<IAuthRemoteDataSource>(
    () => AuthRemoteDataSource(globalAuthState: sl())
  );

  sl.registerLazySingleton<IAuthLocalDataSource>(
    () => AuthLocalDataSource(
      globalAuthState: sl<GlobalAuthState>(),
      sharedPreferences: sl<SharedPreferences>()
    )
  );

  sl.registerLazySingleton<IVideoRepository>(
    () => VideoRepository(remoteDataSource: sl())
  );
  sl.registerLazySingleton<IAuthRepository>(
    () => AuthRepository(remoteDataSource: sl(), localDataSource: sl())
  );

  sl.registerLazySingleton(() => AllVideos(videoRepository: sl()));
  sl.registerLazySingleton(() => Auth(authRepository: sl()));
  sl.registerLazySingleton(() => GetVideo(videoRepository: sl()));
  sl.registerLazySingleton(() => SignUp(authRepository: sl()));
  sl.registerLazySingleton(() => CreateComment(videoRepository: sl()));
  sl.registerLazySingleton(() => GetComment(videoRepository: sl()));
  sl.registerLazySingleton(() => GetUserVideos(videoRepository: sl()));
  sl.registerLazySingleton(() => CreateVideo(videoRepository: sl()));
  sl.registerLazySingleton(() => Logout(authRepository: sl()));
  sl.registerLazySingleton(() => GetUser(authRepository: sl()));
  sl.registerLazySingleton(() => GetRelatedVideos(videoRepository: sl()));
  sl.registerLazySingleton(() => DeleteComment(videoRepository: sl()));
  sl.registerLazySingleton(() => UpdateComment(videoRepository: sl()));
  sl.registerLazySingleton(() => DeleteVideo(videoRepository: sl()));
  sl.registerLazySingleton(() => UpdateVideo(videoRepository: sl()));

  sl.registerLazySingleton(() => GlobalAuthState());

  final sharedPreferences = await SharedPreferences.getInstance();
  sl.registerLazySingleton(() => sharedPreferences);
}