class AppRoutes{

  static const String login = '/login';
  static const String signUp = '/sign_up';
  static const String flow = '/flow';
  static const String flowVideo = '/flow/video';
  static const String user = '/user';
  static const String userVideo = '/user/video';
  static const String createVideo = '/user/create_video';
  
}