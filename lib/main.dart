import 'package:bloc_example/data/auth/datasourse/auth_local_data_source.dart';
import 'package:bloc_example/di/auth/login_cubit/login_cubit.dart';
import 'package:bloc_example/firebase_options.dart';
import 'package:bloc_example/router.dart';
import 'package:bloc_example/service_locator.dart';
import 'package:bloc_example/theme.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'configure_nonweb.dart' if (dart.library.html) 'configure_web.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  configureApp();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform
  );
  await init();
  sl<IAuthLocalDataSource>().getToken();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<LoginCubit>(
      create: (context) => sl<LoginCubit>()..checkToken(),
      child: MediaQuery(
        data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
        child: MaterialApp.router(
          title: 'Example video player with BLoC',
          theme: appTheme,
          routeInformationParser: goRouter.routeInformationParser,
          routerDelegate: goRouter.routerDelegate,
          routeInformationProvider: goRouter.routeInformationProvider,
        ),
      ),
    );
  }
}
