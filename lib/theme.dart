import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

final ThemeData appTheme = ThemeData(
  pageTransitionsTheme: PageTransitionsTheme(
    builders: kIsWeb ? {
      for(final platform in TargetPlatform.values)
        platform: const NoTransitionBuilder()
    } : {
      for(final platform in TargetPlatform.values)
        platform: const CupertinoPageTransitionsBuilder()
    }
  ),
  platform: TargetPlatform.iOS,
  primarySwatch: Colors.purple,
  appBarTheme: const AppBarTheme(
    elevation: 0,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.vertical(
        bottom: Radius.circular(16)
      ),
    )
  ),
  inputDecorationTheme: const InputDecorationTheme(
    floatingLabelStyle:  TextStyle(color: Colors.purple),
    border: OutlineInputBorder(
      borderRadius:  BorderRadius.all(Radius.circular(12)),
      borderSide: BorderSide(
        color: Colors.purple,
      )
    ),
    focusedBorder: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(12)),
      borderSide: BorderSide(
        color: Colors.purple,
      )
    ),
    enabledBorder: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(12)),
      borderSide: BorderSide(
        color: Colors.purple,
      )
    ),
    disabledBorder: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(12)),
      borderSide: BorderSide(
        color: Colors.purple,
      )
    ),
    errorBorder: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(12)),
      borderSide: BorderSide(
        color: Colors.purple,
      )
    ),
    focusedErrorBorder: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(12)),
      borderSide: BorderSide(
        color: Colors.purple,
      )
    ),
  ),
);

class NoTransitionBuilder extends PageTransitionsBuilder {
  const NoTransitionBuilder();
  @override
  Widget buildTransitions<T>(PageRoute<T> route, BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
    return child;
  }
}