import 'package:bloc_example/app_routes.dart';
import 'package:bloc_example/model/video_model.dart';
import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:video_player/video_player.dart';
import 'package:go_router/go_router.dart';

class VideoItem extends StatefulWidget{
  const VideoItem({Key? key, required this.model}) : super(key: key);
  final VideoModel model;

  @override
  State<VideoItem> createState() => _VideoItemState();
}

class _VideoItemState extends State<VideoItem> {
  late VideoPlayerController controller;
  late ChewieController chewieController;
  final double aspectRatio = 16 / 9;

  @override
  void initState() {
    controller = VideoPlayerController.network(widget.model.path);
    controller.setLooping(true);
    controller.setVolume(0);
    chewieController = ChewieController(
      allowedScreenSleep: false,
      deviceOrientationsAfterFullScreen: [
        DeviceOrientation.portraitUp,
      ],
      videoPlayerController: controller,
      aspectRatio: aspectRatio,
      autoInitialize: true,
      showOptions: false,
      showControls: false,
    );
    super.initState();
  }

  @override
  void dispose() {
    chewieController.dispose();
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onLongPress: ()=> controller.value.isPlaying ?
        controller.pause() : controller.play(),
      onTap: (){
        context.go(context.namedLocation(AppRoutes.flowVideo,
                  pathParameters: <String, String>{'id': widget.model.id.toString()}));
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 6.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            AspectRatio(
              aspectRatio: aspectRatio,
              child: ClipRRect(
                borderRadius: const BorderRadius.all(Radius.circular(4)),
                child: Chewie(controller: chewieController)
              ),
            ),
            VideoProgressIndicator(controller, allowScrubbing: true),
            const SizedBox(height: 6),
            Text(
              widget.model.name,
              style: const TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w700
              ),
            ),
            const SizedBox(height: 8),
            Text(
              widget.model.description,
              style: const TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w500
              ),
            ),
          ],
        ),
      ),
    );
  }
}