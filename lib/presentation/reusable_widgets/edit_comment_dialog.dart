import 'package:bloc_example/di/comments/comments_cubit.dart';
import 'package:bloc_example/service_locator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

showEditCommentDialog({
  required CommentsCubit cubit,
  required BuildContext context,
  required int id,
  required int videoId,
  required String comment
}){
  TextEditingController commentController = TextEditingController();
  commentController.text = comment;
  showDialog(
    context: context, 
    builder: (context) => AlertDialog(
      title: const Text('Edit comment'),
      content: SizedBox(
        width: 400,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            TextField(
              controller: commentController,
            ),
            const SizedBox(height: 8),
            BlocProvider<CommentsCubit>(
              create: (context) => CommentsCubit(
                deleteComment: sl(),
                getComment: sl(),
                createComment: sl(),
                updateComment: sl()
              ),
              child: ElevatedButton(onPressed: (){
                Navigator.of(context).pop();
                cubit.updateCommentEv(id: id, comment: commentController.text, videoId: videoId);
              }, child: const Text('Edit')),
            )
          ],
        ),
      )
    )
  );
}