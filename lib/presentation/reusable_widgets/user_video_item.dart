import 'package:bloc_example/app_routes.dart';
import 'package:bloc_example/model/user_data.dart';
import 'package:bloc_example/model/video_model.dart';
import 'package:bloc_example/presentation/mobile/home/widget/edit_video_dialog.dart';
import 'package:go_router/go_router.dart';
import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:video_player/video_player.dart';

class UserVideoItem extends StatefulWidget{
  const UserVideoItem({Key? key, required this.model, required this.deleteVideo, required this.userData}) : super(key: key);
  final VideoModel model;
  final VoidCallback deleteVideo;
  final UserData userData;

  @override
  State<UserVideoItem> createState() => _UserVideoItemState();
}

class _UserVideoItemState extends State<UserVideoItem> {
  final double aspectRatio = 16 / 9;
  late VideoPlayerController controller;
  late ChewieController chewieController;

  @override
  void initState() {
    controller = VideoPlayerController.network(widget.model.path);
    controller.setLooping(true);
    controller.setVolume(0);
    chewieController = ChewieController(
      allowedScreenSleep: false,
      deviceOrientationsAfterFullScreen: [
        DeviceOrientation.portraitUp,
      ],
      videoPlayerController: controller,
      aspectRatio: aspectRatio,
      autoInitialize: true,
      showOptions: false,
      showControls: false,
    );
    super.initState();
  }

  @override
  void dispose() {
    chewieController.dispose();
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onLongPress: ()=> controller.value.isPlaying ?
        controller.pause() : controller.play(),
      onTap: (){
        context.go(context.namedLocation(AppRoutes.userVideo,
                  pathParameters: <String, String>{'id': widget.model.id.toString()}));
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 6.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            AspectRatio(
              aspectRatio: aspectRatio,
              child: ClipRRect(
                borderRadius: const BorderRadius.all(Radius.circular(4)),
                child: Chewie(controller: chewieController)
              ),
            ),
            VideoProgressIndicator(controller, allowScrubbing: true),
            const SizedBox(height: 6),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  widget.model.name,
                  style: const TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w700
                  ),
                ),
                IconButton(
                  onPressed: (){
                    showDialog(
                      context: context, 
                      builder: (context) => Dialog(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            ListTile(
                              title: const Text('Edit'),
                              leading: const Icon(Icons.edit),
                              onTap: (){
                                Navigator.of(context).pop();
                                showEditVideoDialog(
                                  context: context, 
                                  id: widget.model.id, 
                                  name: widget.model.name, 
                                  description: widget.model.description, 
                                  userData: widget.userData
                                );
                              },
                            ),
                            ListTile(
                              title: const Text('Delete'),
                              leading: const Icon(Icons.delete),
                              onTap: widget.deleteVideo,
                            )
                          ],
                        ),
                      )
                    );
                  }, 
                  icon: const Icon(Icons.more_vert_rounded)
                )
              ],
            ),
            Text(
              widget.model.description,
              style: const TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w500
              ),
            ),
          ],
        ),
      ),
    );
  }
}