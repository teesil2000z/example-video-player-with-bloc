import 'package:bloc_example/app_routes.dart';
import 'package:bloc_example/model/user_data.dart';
import 'package:bloc_example/model/video_model.dart';
import 'package:bloc_example/presentation/web/home/widgets/web_edit_video_dialog.dart';
import 'package:go_router/go_router.dart';
import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:video_player/video_player.dart';

class WebUserVideoItem extends StatefulWidget{
  const WebUserVideoItem({Key? key, required this.model, required this.deleteVideo, required this.userData}) : super(key: key);
  final VideoModel model;
  final VoidCallback deleteVideo;
  final UserData userData;

  @override
  State<WebUserVideoItem> createState() => _WebUserVideoItemState();
}

class _WebUserVideoItemState extends State<WebUserVideoItem> {
  late VideoPlayerController controller;
  late ChewieController chewieController;
  final double aspectRatio = 16 / 9;

  @override
  void initState() {
    controller = VideoPlayerController.network(widget.model.path);
    controller.setLooping(true);
    controller.setVolume(0);
    chewieController = ChewieController(
      allowedScreenSleep: false,
      deviceOrientationsAfterFullScreen: [
        DeviceOrientation.portraitUp,
      ],
      videoPlayerController: controller,
      aspectRatio: aspectRatio,
      autoInitialize: true,
      showOptions: false,
      showControls: false,
    );
    super.initState();
  }

  @override
  void dispose() {
    chewieController.dispose();
    controller.dispose();
    super.dispose();
  }
  
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 250,
      child: InkWell(
        onLongPress: ()=> controller.value.isPlaying ?
          controller.pause() : controller.play(),
        onTap: (){
          context.push(context.namedLocation(AppRoutes.userVideo,
                  pathParameters: <String, String>{'id': widget.model.id.toString()}));
        },
        child: Padding(
          padding: const EdgeInsets.only(left: 8.0, right: 8.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AspectRatio(
                aspectRatio: aspectRatio,
                child: ClipRRect(
                  borderRadius: const BorderRadius.all(Radius.circular(4)),
                  child: Chewie(controller: chewieController)
                ),
              ),
              VideoProgressIndicator(controller, allowScrubbing: true),
              const SizedBox(height: 6),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    widget.model.name,
                    style: const TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w700
                    ),
                  ),
                  IconButton(
                    onPressed: (){
                      showDialog(
                        context: context, 
                        builder: (context) => Dialog(
                          child: SizedBox(
                            width: 400,
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                ListTile(
                                  title: const Text('Edit'),
                                  leading: const Icon(Icons.edit),
                                  onTap: (){
                                    Navigator.of(context).pop();
                                    showWebEditVideoDialog(
                                      context: context, 
                                      id: widget.model.id, 
                                      name: widget.model.name, 
                                      description: widget.model.description, 
                                      userData: widget.userData
                                    );
                                  },
                                ),
                                ListTile(
                                  title: const Text('Delete'),
                                  leading: const Icon(Icons.delete),
                                  onTap: widget.deleteVideo,
                                )
                              ],
                            ),
                          ),
                        )
                      );
                    }, 
                    icon: const Icon(Icons.more_vert_rounded)
                  )
                ],
              ),
              Text(
                widget.model.description,
                style: const TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w500
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}