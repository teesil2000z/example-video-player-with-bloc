import 'package:bloc_example/app_routes.dart';
import 'package:bloc_example/data/auth/usecases/sign_up.dart';
import 'package:bloc_example/di/auth/sign_up_cubit/sign_up_cubit.dart';
import 'package:bloc_example/service_locator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

class SignUpPage extends StatelessWidget {

  const SignUpPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Scaffold(
          appBar: AppBar(
            title: const Text('Sign up'),
          ),
          body: BlocProvider<SignUpCubit>(create: (context)=> SignUpCubit(signUp: sl<SignUp>()),
            child: BlocConsumer<SignUpCubit, SignUpState>(
              listener: (context, state) {
                if (state.signUpBechaviorEnum == SignUpBechaviorEnum.signedUp) {
                  ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(content: Text('User created')));
                  context.read<SignUpCubit>().toInit();
                }
                if (state.signUpBechaviorEnum == SignUpBechaviorEnum.error) {
                  ScaffoldMessenger.of(context)
                      .showSnackBar(SnackBar(content: Text(state.errorMessage)));
                  context.read<SignUpCubit>().toInit();
                }
              },
              builder: (context, state) {
                if (state.signUpBechaviorEnum == SignUpBechaviorEnum.loading) {
                  return const Center(
                    child: CircularProgressIndicator.adaptive(),
                  );
                } else {
                  return _Content(state: state);
                }
              }
            ),
          )),
    );
  }
}

class _Content extends StatelessWidget {
  final SignUpState state;
  const _Content({required this.state, Key? key,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: TextFormField(
                initialValue: state.name,
                onChanged: (value) => context.read<SignUpCubit>().setName(value),
                keyboardType: TextInputType.emailAddress,
                decoration: const InputDecoration(hintText: 'Name'),
              ),
            ),
            const SizedBox(height: 16),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: TextFormField(
                initialValue: state.email,
                onChanged: (value) => context.read<SignUpCubit>().setEmail(value),
                decoration: const InputDecoration(hintText: 'Email'),
              ),
            ),
            const SizedBox(height: 16),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: TextFormField(
                initialValue: state.password,
                onChanged: (value) => context.read<SignUpCubit>().setPassword(value),
                obscureText: true,
                decoration: const InputDecoration(hintText: 'Password'),
              ),
            ),
            const SizedBox(height: 16),
            ElevatedButton(
                child: const Text('Sign up'),
                onPressed: () {
                  if (state.email.isNotEmpty &&
                      state.password.isNotEmpty &&
                      state.name.isNotEmpty) {
                    context.read<SignUpCubit>().performSignUp();
                  } else {
                    Future.microtask(() {
                      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                          content: Text('All fields are required!')));
                    });
                  }
                }),
            ElevatedButton(
                onPressed: () => context.go(AppRoutes.login),
                child: const Text('To sign in'))
          ],
        ),
      ),
    );
  }
}
