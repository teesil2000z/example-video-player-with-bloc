import 'package:bloc_example/app_routes.dart';
import 'package:bloc_example/di/auth/login_cubit/login_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

class LoginPage extends StatelessWidget {

  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Scaffold(
          appBar: AppBar(
            title: const Text('Log in'),
          ),
          body: BlocConsumer<LoginCubit, LoginState>(
            listener: (context, state) {
              if (state.loginBechaviorEnum == LoginBechaviorEnum.error) {
                ScaffoldMessenger.of(context)
                    .showSnackBar(SnackBar(content: Text(state.errorMessage)));
                context.read<LoginCubit>().toInit();
              }
              if (state.loginBechaviorEnum == LoginBechaviorEnum.authorized) {
                context.go(AppRoutes.flow);
                context.read<LoginCubit>().toInit();
              }
            },
            builder: (context, state) {
              if (state.loginBechaviorEnum == LoginBechaviorEnum.loading) {
                return const Center(
                  child: CircularProgressIndicator.adaptive(),
                );
              } else{
                return const _Content();
              }
            }
          )
        ),
    );
  }
}

class _Content extends StatelessWidget {
  const _Content({Key? key,}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return Center(
      child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: TextFormField(
                initialValue: '',
                onChanged: (value) => context.read<LoginCubit>().setEmail(value),
                keyboardType: TextInputType.emailAddress,
                decoration: const InputDecoration(hintText: 'Email'),
              ),
            ),
            const SizedBox(height: 16),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: TextFormField(
                initialValue: '',
                onChanged: (value) => context.read<LoginCubit>().setPassword(value),
                obscureText: true,
                decoration: const InputDecoration(hintText: 'Password'),
              ),
            ),
            const SizedBox(height: 16),
            ElevatedButton(
                child: const Text('Sign in'),
                onPressed: () => context.read<LoginCubit>().login()),
            const SizedBox(height: 8),
            ElevatedButton(
                onPressed: () => context.go(AppRoutes.signUp),
                child: const Text('To sign up'))
          ],
        ),
      ),
    );
  }
}
