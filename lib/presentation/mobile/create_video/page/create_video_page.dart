import 'dart:io';

import 'package:bloc_example/di/create_video/controller_cubit/controller_cubit.dart';
import 'package:bloc_example/di/create_video/create_video_bloc/create_video_cubit.dart';
import 'package:bloc_example/di/home/flow_bloc/flow_bloc.dart';
import 'package:bloc_example/di/home/flow_bloc/flow_event.dart';
import 'package:bloc_example/di/home/user_bloc/user_bloc.dart';
import 'package:bloc_example/di/home/user_bloc/user_event.dart';
import 'package:bloc_example/presentation/mobile/create_video/widget/uploading_video_item.dart';
import 'package:bloc_example/presentation/reusable_widgets/loading_widget.dart';
import 'package:bloc_example/service_locator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';

// ignore: must_be_immutable
class CreateVideoPage extends StatelessWidget {
  CreateVideoPage({Key? key}) : super(key: key);
  final ImagePicker picker = ImagePicker();

  File? video;
  static const double aspectRatio = 16 / 9;

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<VideoControllerCubit>(create: ((context) => VideoControllerCubit())),
        BlocProvider<CreateVideoCubit>(create: ((context) => CreateVideoCubit(createVideo: sl()))),
      ],
      child: ColoredBox(
        color: Colors.white,
        child: BlocConsumer<CreateVideoCubit, CreateVideoState>(
          listener: (context, state) {
            if (state.createVideoAct == CreateVideoAct.created) {
              context.read<FlowBloc>().add(FlowLoadEvent());
              context.read<UserBloc>().add(LoadUserEvent());
              context.read<CreateVideoCubit>().toInit();
              Navigator.of(context).pop();
              ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                  content: Text('Video successfuly uploaded')));
            } if (state.createVideoAct == CreateVideoAct.error) {
              ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(content: Text('Something went wrong!')));
              context.read<CreateVideoCubit>().toInit();
            }
          },
          builder: ((context, state) {
            if (state.createVideoAct == CreateVideoAct.init || state.createVideoAct == CreateVideoAct.error || state.createVideoAct == CreateVideoAct.created) {
              return _Content(state: state);
            } else if (state.createVideoAct == CreateVideoAct.loading) {
              return const LoadingWidget();
            } else {
              return const LoadingWidget();
            }
          }),
        ),
      ),
    );
  }
}

class _Content extends StatelessWidget{
  final CreateVideoState state;
  const _Content({Key? key, required this.state}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    File? video;
    ImagePicker picker = ImagePicker();
    void videoFromGallery(BuildContext context) async {
      XFile? pickedFile = await picker.pickVideo(source: ImageSource.gallery);
      if (pickedFile != null) {
        video = File(pickedFile.path);
        Future.microtask(() => context.read<VideoControllerCubit>().changePlayerState(VideoControllerState.loaded));
      }
    }
    return Center(
      child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: TextFormField(
                initialValue: state.title,
                onChanged: (value) => context.read<CreateVideoCubit>().setTitle(value),
                keyboardType: TextInputType.emailAddress,
                decoration: const InputDecoration(hintText: 'Titile'),
              ),
            ),
            const SizedBox(height: 16),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: TextFormField(
                initialValue: state.descriprion,
                onChanged: (value) => context.read<CreateVideoCubit>().setDescription(value),
                decoration: const InputDecoration(hintText: 'Description'),
              ),
            ),
            const SizedBox(height: 16),
            BlocBuilder<VideoControllerCubit, VideoControllerState>(
                builder: (context, state) {
              if (state == VideoControllerState.loaded) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    UploadingVideoItem(data: video!),
                    ElevatedButton(
                        child: const Text('Upload'),
                        onPressed: () {
                          if (this.state.title.isNotEmpty &&
                              this.state.descriprion.isNotEmpty &&
                              video != null) {
                            context.read<CreateVideoCubit>().createVideoAct(video: video!);
                          } else {
                            Future.microtask(() {
                              ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(
                                      content:
                                          Text('All fields are required!')));
                            });
                          }
                        })
                  ],
                );
              } else {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    ElevatedButton(
                        child: const Text('Add video'),
                        onPressed: () {
                          videoFromGallery(context);
                        }),
                    const ElevatedButton(onPressed: null, child: Text('Upload'))
                  ],
                );
              }
            }),
          ],
        ),
      ),
    );
  }
}
