import 'package:bloc_example/di/home/user_bloc/user_bloc.dart';
import 'package:bloc_example/di/home/user_bloc/user_event.dart';
import 'package:bloc_example/model/user_data.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

showEditVideoDialog({
  required BuildContext context,
  required int id,
  required String name,
  required String description,
  required UserData userData
}){
  TextEditingController nameController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  nameController.text = name;
  descriptionController.text = description;
  showDialog(
    context: context, 
    builder: (context) => AlertDialog(
      title: const Text('Edit video'),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          TextField(
            decoration: const InputDecoration(hintText: 'Name'),
            controller: nameController,
          ),
          const SizedBox(
            height: 8,
          ),
          TextField(
            decoration: const InputDecoration(hintText: 'Description'),
            controller: descriptionController,
          ),
          const SizedBox(height: 8),
          ElevatedButton(onPressed: (){
            Navigator.of(context).pop();
            context.read<UserBloc>().add(UpdateVideoEvent(id: id, name: nameController.text, description: descriptionController.text, userData: userData));
          }, child: const Text('Edit'))
        ],
      )
    )
  ).then((value) {
    nameController.dispose();
    descriptionController.dispose();
  });
}