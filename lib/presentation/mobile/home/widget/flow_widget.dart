import 'package:bloc_example/di/home/flow_bloc/flow_bloc.dart';
import 'package:bloc_example/di/home/flow_bloc/flow_event.dart';
import 'package:bloc_example/di/home/flow_bloc/flow_state.dart';
import 'package:bloc_example/presentation/reusable_widgets/loading_widget.dart';
import 'package:bloc_example/presentation/reusable_widgets/video_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FlowWidget extends StatelessWidget {
  const FlowWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FlowBloc, FlowState>(builder: (context, state) {
      if (state is FlowLoadedState) {
        return ListView.builder(
            itemCount: state.data.length,
            itemBuilder: (context, i) => VideoItem(model: state.data[i]));
      } else if (state is FlowErrorState) {
        return const Center(child: Text('Something went wrong'));
      } else if (state is FlowInitState) {
        context.read<FlowBloc>().add(FlowLoadEvent());
        return const LoadingWidget();
      } else {
        return const LoadingWidget();
      }
    });
  }
}
