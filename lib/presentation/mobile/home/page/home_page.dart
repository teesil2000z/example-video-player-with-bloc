import 'package:bloc_example/app_routes.dart';
import 'package:bloc_example/di/auth/login_cubit/login_cubit.dart';
import 'package:bloc_example/di/home/flow_bloc/flow_bloc.dart';
import 'package:bloc_example/di/home/navigation_cubit/navigation_cubit.dart';
import 'package:bloc_example/di/home/user_bloc/user_bloc.dart';
import 'package:bloc_example/service_locator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key, required this.child}) : super(key: key);
  final Widget child;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: const Text('Video player'),
        actions: [
          IconButton(
            onPressed: () {
              context.read<LoginCubit>().logOut();
              Future.microtask(() => context.go(AppRoutes.login));
            },
            icon: const Icon(Icons.logout)
          )
        ],
      ),
      body: MultiBlocProvider(
        providers: [
          BlocProvider<FlowBloc>(create: (context) => sl<FlowBloc>()),
          BlocProvider<UserBloc>(create: (context) => sl<UserBloc>()),
        ],
        child: child
      ),
      bottomNavigationBar: BlocProvider<NavigationCubit>(create: (context)=> NavigationCubit(),
        child: BlocBuilder<NavigationCubit, NavigationSelection>(builder: (context, state) {
          return BottomNavigationBar(
            currentIndex: state == NavigationSelection.flow ? 0 : 1,
            items: const [
              BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Flow'),
              BottomNavigationBarItem(icon: Icon(Icons.person), label: 'User')
            ],
            onTap: (i) {
              if (i == 0) {
                context.read<NavigationCubit>().changeSelection(NavigationSelection.flow);
                context.go(AppRoutes.flow);
              } else {
                context.read<NavigationCubit>().changeSelection(NavigationSelection.user);
                context.go(AppRoutes.user);
              }
            },
          );
        }),
      )
    );
  }
}
