import 'package:bloc_example/di/video/video_bloc.dart';
import 'package:bloc_example/di/video/video_event.dart';
import 'package:bloc_example/di/video/video_state.dart';
import 'package:bloc_example/presentation/mobile/comments/page/comments_page.dart';
import 'package:bloc_example/presentation/mobile/comments/widget/comments_bottom_sheet.dart';
import 'package:bloc_example/presentation/reusable_widgets/video_item.dart';
import 'package:bloc_example/service_locator.dart';
import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:video_player/video_player.dart';

class VideoPage extends StatelessWidget {
  const VideoPage({
    Key? key,
    required this.id
  }) : super(key: key);
  final int id;
  @override
  Widget build(BuildContext context) {
    return BlocProvider<VideoBloc>(create: (context) => VideoBloc(
        allVideos: sl(),
        getVideo: sl()
      )..add(VideoLoadEvent(id: id)),
      child: BlocBuilder<VideoBloc, VideoState>(builder: ((context, state) {
        if (state is VideoLoadedState) {
          const double aspectRatio = 16 / 9;
          final VideoPlayerController controller =
              VideoPlayerController.network(state.model.path);
          ChewieController chewieController = ChewieController(
            allowedScreenSleep: false,
            allowFullScreen: true,
            deviceOrientationsAfterFullScreen: [
              DeviceOrientation.portraitUp,
            ],
            videoPlayerController: controller,
            aspectRatio: aspectRatio,
            autoInitialize: true,
            autoPlay: true,
            showControls: true,
          );
          return ColoredBox(
            color: Colors.white,
            child: WillPopScope(
              onWillPop: () async {
                controller.dispose();
                chewieController.dispose();
                return true;
              },
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AspectRatio(
                    aspectRatio: aspectRatio,
                    child: Chewie(controller: chewieController),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 12, right: 12, top: 6),
                    child: Text(
                      state.model.name,
                      style: const TextStyle(
                          fontSize: 20, fontWeight: FontWeight.w700),
                    ),
                  ),
                  const SizedBox(height: 8),
                  Padding(
                    padding: const EdgeInsets.only(left: 12, right: 12, bottom: 6),
                    child: Text(
                      state.model.description,
                      style: const TextStyle(
                          fontSize: 16, fontWeight: FontWeight.w500),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      showCommentsModalBottomSheet(
                          expand: true,
                          context: context,
                          backgroundColor: Colors.transparent,
                          builder: (context) => CommentsPage(id: state.model.id));
                    },
                    child: SizedBox(
                      height: 40,
                      child: DecoratedBox(
                        decoration: BoxDecoration(
                          color: Colors.grey.shade300,
                          borderRadius:
                              const BorderRadius.all(Radius.circular(12))),
                        child: const Padding(
                          padding: EdgeInsets.symmetric(horizontal: 12, vertical: 6),
                          child: Center(
                              child: Text(
                            'Comments',
                            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                          )),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                      child: ListView.builder(
                          itemCount: state.data.length,
                          itemBuilder: (context, i) => VideoItem(
                                model: state.data[i],
                              )))
                ],
              ),
            ),
          );
        } else if (state is VideoErrorState) {
          return const Center(
            child: Text('Something went wrong!'),
          );
        } else {
          return const Center(
            child: CircularProgressIndicator.adaptive(),
          );
        }
      })),
    );
  }
}
