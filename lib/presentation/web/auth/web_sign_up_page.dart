import 'package:bloc_example/app_routes.dart';
import 'package:bloc_example/data/auth/usecases/sign_up.dart';
import 'package:bloc_example/di/auth/sign_up_cubit/sign_up_cubit.dart';
import 'package:bloc_example/service_locator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

class WebSignUpPage extends StatelessWidget {

  const WebSignUpPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade100,
      body: BlocProvider<SignUpCubit>(create: (context)=> SignUpCubit(signUp: sl<SignUp>()),
        child: BlocConsumer<SignUpCubit, SignUpState>(
          listener: (context, state) {
            if (state.signUpBechaviorEnum == SignUpBechaviorEnum.signedUp) {
              ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(content: Text('User created')));
              context.read<SignUpCubit>().toInit();
            }
            if (state.signUpBechaviorEnum == SignUpBechaviorEnum.error) {
              ScaffoldMessenger.of(context)
                  .showSnackBar(SnackBar(content: Text(state.errorMessage)));
              context.read<SignUpCubit>().toInit();
            }
          },
          builder: (context, state) {
            if (state.signUpBechaviorEnum == SignUpBechaviorEnum.loading) {
              return const Center(
                child: CircularProgressIndicator.adaptive(),
              );
            } else {
              return _SignUpContent(state: state);
            }
          }
        )
      )
    );
  }

  
}
class _SignUpContent extends StatelessWidget {
  final SignUpState state;
  const _SignUpContent({Key? key, required this.state}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Center(
      child: DecoratedBox(
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(16))
        ),
        child: SizedBox(
          width: 400,
          child: Padding(
            padding: const EdgeInsets.all(32),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: TextField(
                    onChanged: (value) => context.read<SignUpCubit>().setName(value),
                    keyboardType: TextInputType.emailAddress,
                    decoration: const InputDecoration(hintText: 'Name'),
                  ),
                ),
                const SizedBox(height: 16),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: TextField(
                    onChanged: (value) => context.read<SignUpCubit>().setEmail(value),
                    decoration: const InputDecoration(hintText: 'Email'),
                  ),
                ),
                const SizedBox(height: 16),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: TextField(
                    onChanged: (value) => context.read<SignUpCubit>().setPassword(value),
                    obscureText: true,
                    decoration: const InputDecoration(hintText: 'Password'),
                  ),
                ),
                const SizedBox(height: 16),
                ElevatedButton(
                    child: const Text('Sign up'),
                    onPressed: () {
                      if (state.email.isNotEmpty &&
                        state.password.isNotEmpty &&
                        state.name.isNotEmpty) {
                        context.read<SignUpCubit>().performSignUp();
                      } else {
                        Future.microtask(() {
                          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                              content: Text('All fields are required!')));
                        });
                      }
                    }),
                const SizedBox(height: 12),
                ElevatedButton(
                    onPressed: () => context.go(AppRoutes.login),
                    child: const Text('To sign in'))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
