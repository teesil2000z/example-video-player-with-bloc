import 'package:bloc_example/di/comments/comments_cubit.dart';
import 'package:bloc_example/presentation/reusable_widgets/edit_comment_dialog.dart';
import 'package:bloc_example/service_locator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

class WebCommentsPage extends StatelessWidget {
  const WebCommentsPage({Key? key, required this.id}) : super(key: key);
  final int id;
  @override
  Widget build(BuildContext context) {
    CommentsCubit cubit = CommentsCubit(
      deleteComment: sl(),
      getComment: sl(),
      createComment: sl(),
      updateComment: sl()
    );
    return Provider<CommentsCubit>(
      create: (context) => cubit..loadComments(id),
      builder:(context, child) => GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: Material(
          child: CupertinoPageScaffold(
            backgroundColor: Colors.white,
            child: SafeArea(
              bottom: false,
              child: Padding(
                padding: const EdgeInsets.all(12),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    const Padding(
                      padding: EdgeInsets.only(bottom: 12),
                      child: Text(
                        'Comments:',
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w500),
                      )
                    ),
                    Expanded(child:
                      BlocBuilder<CommentsCubit, CommentsState>(
                        builder: (context, state) {
                          if (state is CommentsLoadingState) {
                            return const Center(
                              child: CircularProgressIndicator.adaptive(),
                            );
                          }else if(state is CommentsErrorState){
                            return const Center(
                              child: Text('Something went wrong')
                            );
                          }else if (state is CommentAddedState){
                            return _Content(state: state, cubit: cubit, id: id);
                          }else{
                            return const SizedBox();
                          }
                        }
                      )
                    ),
                    const SizedBox(height: 8),
                    Row(
                      children: [
                        Expanded(
                          child: TextField(
                            decoration: const InputDecoration(
                                hintText: 'Comment:'),
                            onChanged: (value) => context.read<CommentsCubit>().setComment(value),
                          ),
                        ),
                        IconButton(
                          onPressed: () => context.read<CommentsCubit>().addComment(id: id),
                          icon: const Icon(Icons.send)
                        ),
                      ],
                    ),
                    const SizedBox(height: 12)
                  ]
                )
              )
            )
          )
        ),
      ),
    );
  }
}

class _Content extends StatelessWidget{
  final CommentAddedState state;
  final CommentsCubit cubit;
  final int id;
  const _Content({required this.state, required this.cubit, required this.id, Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: state.comments.length,
      itemBuilder: (context, i) => Padding(
        padding: const EdgeInsets.all(12),
        child: Column(
          crossAxisAlignment:
              CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  state.comments[i].name,
                  style: const TextStyle(
                      fontSize: 16,
                      fontWeight:
                          FontWeight.w600),
                ),
                state.comments[i].isMine ? IconButton(onPressed: (){
                  showDialog(
                    context: context, 
                    builder: (context) => Dialog(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          ListTile(
                            title: const Text('Edit'),
                            leading: const Icon(Icons.edit),
                            onTap: (){
                              Navigator.of(context).pop();
                              showEditCommentDialog(
                                cubit: cubit,
                                context: context, 
                                id: state.comments[i].id, 
                                videoId: state.comments[i].videoId, 
                                comment: state.comments[i].comment
                              );
                            },
                          ),
                          ListTile(
                            title: const Text('Delete'),
                            leading: const Icon(Icons.delete),
                            onTap: (){
                              cubit.deleteCommentEv(commentId: state.comments[i].id, id: id);
                              Navigator.of(context).pop();
                            },
                          )
                        ],
                      ),
                    ) 
                  );
                }, icon: const Icon(Icons.more_vert_rounded)) : const SizedBox()
              ],
            ),
            Text(state.comments[i].comment),
          ],
        ),
      )
    );
  }
}
