import 'package:bloc_example/di/video/video_bloc.dart';
import 'package:bloc_example/di/video/video_event.dart';
import 'package:bloc_example/di/video/video_state.dart';
import 'package:bloc_example/presentation/web/comments/page/comments_page.dart';
import 'package:bloc_example/presentation/web/comments/widget/comments_bottom_sheet.dart';
import 'package:bloc_example/presentation/web/not_found/not_found_page.dart';
import 'package:bloc_example/presentation/reusable_widgets/web_video_item.dart';
import 'package:bloc_example/service_locator.dart';
import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:video_player/video_player.dart';

class WebVideoPage extends StatelessWidget {
  const WebVideoPage({
    Key? key,
    required this.id
  }) : super(key: key);
  final int id;
  @override
  Widget build(BuildContext context) {
    return BlocProvider<VideoBloc>(create: (context) => VideoBloc(
        allVideos: sl(),
        getVideo: sl()
      )..add(VideoLoadEvent(id: id)),
      child: BlocBuilder<VideoBloc, VideoState>(builder: ((context, state) {
        if (state is VideoLoadedState) {
          if(state.model.id!=0){
            return content(context, state);
          }else{
            return const PageNotFound();
          }
        } else if (state is VideoErrorState) {
          return const Center(
            child: Text('Something went wrong!'),
          );
        } else {
          return const Center(
            child: CircularProgressIndicator.adaptive(),
          );
        }
      })),
    );
  }
  Widget content(BuildContext context, VideoLoadedState state){
    const double aspectRatio = 16 / 9;
    final VideoPlayerController controller = VideoPlayerController.network(state.model.path);
    ChewieController chewieController = ChewieController(
      allowedScreenSleep: false,
      allowFullScreen: true,
      deviceOrientationsAfterFullScreen: [
        DeviceOrientation.portraitUp,
      ],
      videoPlayerController: controller,
      aspectRatio: aspectRatio,
      autoInitialize: true,
      autoPlay: true,
      showControls: true,
    );
    return ColoredBox(
      color: Colors.white,
      child: WillPopScope(
        onWillPop: () async {
          controller.dispose();
          chewieController.dispose();
          return true;
        },
        child: ListView(
          children: [
            AspectRatio(
              aspectRatio: aspectRatio,
              child: Chewie(controller: chewieController),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 12, right: 12, top: 6),
              child: Text(
                state.model.name,
                style: const TextStyle(
                    fontSize: 20, fontWeight: FontWeight.w700),
              ),
            ),
            const SizedBox(height: 8),
            Padding(
              padding:
                  const EdgeInsets.only(left: 12, right: 12, bottom: 6),
              child: Text(
                state.model.description,
                style: const TextStyle(
                    fontSize: 16, fontWeight: FontWeight.w500),
              ),
            ),
            GestureDetector(
              onTap: () {
                showWebCommentsModalBottomSheet(
                    expand: true,
                    context: context,
                    backgroundColor: Colors.transparent,
                    builder: (context) => WebCommentsPage(id: state.model.id));
              },
              child: SizedBox(
                height: 40,
                child: DecoratedBox(
                  decoration: BoxDecoration(
                    color: Colors.grey.shade300,
                    borderRadius: const BorderRadius.all(Radius.circular(12))
                  ),
                  child: const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 12, vertical: 6),
                    child: Center(
                        child: Text(
                      'Comments',
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                    )),
                  ),
                ),
              ),
            ),
            GridView.count(
                shrinkWrap: true,
                physics: const ClampingScrollPhysics(),
                crossAxisSpacing: 4,
                mainAxisSpacing: 4,
                crossAxisCount: MediaQuery.of(context).size.width ~/ 260,
                children: state.data
                    .map((e) => WebVideoItem(model: e))
                    .toList())
          ],
        ),
      ),
    );
  }
}
