import 'dart:io';

import 'package:bloc_example/di/create_video/controller_cubit/controller_cubit.dart';
import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:video_player/video_player.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class WebUploadingVideoItem extends StatefulWidget{
  const WebUploadingVideoItem({Key? key, required this.data}) : super(key: key);
  final File data;

  @override
  State<WebUploadingVideoItem> createState() => _WebUploadingVideoItemState();
}

class _WebUploadingVideoItemState extends State<WebUploadingVideoItem> {
  late VideoPlayerController controller;
  late ChewieController chewieController;
  final double aspectRatio = 16 / 9;

  @override
  void initState() {
    controller = VideoPlayerController.file(widget.data);
    controller.setLooping(true);
    controller.setVolume(0);
    chewieController = ChewieController(
      allowedScreenSleep: false,
      deviceOrientationsAfterFullScreen: [
        DeviceOrientation.portraitUp,
      ],
      videoPlayerController: controller,
      aspectRatio: aspectRatio,
      autoInitialize: true,
      showOptions: false,
      showControls: false,
    );
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    chewieController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<VideoControllerCubit>(create: ((context) => VideoControllerCubit()),
      child: InkWell(
        onTap: ()=> controller.value.isPlaying ?
          controller.pause() : controller.play(),
        onLongPress: (){
          context.read<VideoControllerCubit>().changePlayerState(VideoControllerState.empty);
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 6.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AspectRatio(
                aspectRatio: aspectRatio,
                child: ClipRRect(
                  borderRadius: const BorderRadius.all(Radius.circular(4)),
                  child: Chewie(controller: chewieController)
                ),
              ),
              VideoProgressIndicator(controller, allowScrubbing: true),
            ],
          ),
        ),
      ),
    );
  }
}