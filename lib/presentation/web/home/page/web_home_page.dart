
import 'package:bloc_example/di/home/flow_bloc/flow_bloc.dart';
import 'package:bloc_example/di/home/navigation_cubit/navigation_cubit.dart';
import 'package:bloc_example/di/home/user_bloc/user_bloc.dart';
import 'package:bloc_example/presentation/web/home/widgets/large_screen.dart';
import 'package:bloc_example/presentation/web/home/widgets/side_menu.dart';
import 'package:bloc_example/presentation/web/home/widgets/top_menu.dart';
import 'package:bloc_example/presentation/web/home/widgets/web_responsive_widget.dart';
import 'package:bloc_example/service_locator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

class WebHomeWidget extends StatelessWidget {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();
  final Widget child;
  WebHomeWidget({Key? key, required this.child}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        BlocProvider<FlowBloc>(create: (context) => sl<FlowBloc>()),
        BlocProvider<UserBloc>(create: (context) => sl<UserBloc>()),
        BlocProvider<NavigationCubit>(create: (context) => NavigationCubit())
      ],
      builder:(context, widget) => Scaffold(
        key: scaffoldKey,
        extendBodyBehindAppBar: true,
        appBar: TopNavigationBar(scaffoldKey: scaffoldKey),
        drawer: const Drawer(
          child: SideMenu(),
        ),
        body: ResponsiveWidget(
          largeScreen: LargeScreen(child: child),
          smallScreen: RepaintBoundary(child: child)
        ),
      ),
    );
  }
}