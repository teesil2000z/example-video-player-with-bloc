import 'package:bloc_example/app_routes.dart';
import 'package:bloc_example/di/home/user_bloc/user_bloc.dart';
import 'package:bloc_example/di/home/user_bloc/user_event.dart';
import 'package:bloc_example/di/home/user_bloc/user_state.dart';
import 'package:bloc_example/presentation/reusable_widgets/loading_widget.dart';
import 'package:bloc_example/presentation/reusable_widgets/web_user_video_item.dart';
import 'package:go_router/go_router.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class WebUserWidget extends StatelessWidget {
  const WebUserWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserBloc, UserState>(builder: ((context, state) {
      if (state is UserInitState) {
        context.read<UserBloc>().add(LoadUserEvent());
        return const LoadingWidget();
      } else if (state is UserLoadingState) {
        return const LoadingWidget();
      } else if (state is UserErrorState) {
        return const Center(child: Text('Something went wrong'));
      } else if (state is UserLoadedState) {
        return _Content(state: state);
      } else {
        return const LoadingWidget();
      }
    }));
  }
}

class _Content extends StatelessWidget{
  final UserLoadedState state;

  const _Content({required this.state, Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height,
      child: Column(children: [
        Center(
            child: Column(
          children: [
            const SizedBox(height: 120),
            Text(
              state.userData.name,
              style: const TextStyle(
                  fontSize: 18, fontWeight: FontWeight.w600),
            ),
            const SizedBox(height: 8),
            Text(state.userData.email),
          ],
        )),
        const SizedBox(height: 16),
        InkWell(
          child: SizedBox(
            height: 50,
            width: MediaQuery.of(context).size.width,
            child: const Center(
              child: Text(
                'Add video',
                style: TextStyle(fontSize: 16, color: Colors.purple),
              ),
            ),
          ),
          onTap: () {
            context.go(AppRoutes.createVideo);
          },
        ),
        const SizedBox(height: 16),
        state.videos.isNotEmpty
          ? Expanded(
            child: GridView.count(
              crossAxisSpacing: 4,
              mainAxisSpacing: 4,
              crossAxisCount: MediaQuery.of(context).size.width ~/ 260,
              children: state.videos
                .map((e) => WebUserVideoItem(
                    model: e,
                    userData: state.userData,
                    deleteVideo: () =>context.read<UserBloc>().add(
                      DeleteVideoEvent(
                        videoId: e.id,
                        userData: state.userData
                      )
                    ),
                )).toList())
              )
        : const Center(child: Text('No videos'))
      ]),
    );
  }
}
