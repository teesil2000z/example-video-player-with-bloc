import 'package:bloc_example/app_routes.dart';
import 'package:bloc_example/di/auth/login_cubit/login_cubit.dart';
import 'package:bloc_example/di/home/navigation_cubit/navigation_cubit.dart';
import 'package:bloc_example/model/global_auth_state.dart';
import 'package:bloc_example/presentation/web/home/widgets/web_responsive_widget.dart';
import 'package:bloc_example/service_locator.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


class TopNavigationBar extends StatelessWidget implements PreferredSizeWidget{
  final GlobalKey<ScaffoldState> scaffoldKey;
  const TopNavigationBar({super.key, required this.scaffoldKey});

  @override
  Widget build(BuildContext context) {
    return AppBar(
      leading: !ResponsiveWidget.isSmallScreen(context)
        ? const ColoredBox(
          color: Colors.white,
        )
        : ColoredBox(
          color: Colors.white,
          child: IconButton(
            icon: const Icon(
              Icons.menu,
              color: Colors.black,
            ),
            onPressed: () {
              scaffoldKey.currentState!.openDrawer();
            }
          ),
        ),
      titleSpacing: 0,
      title: SizedBox(
        height: 60,
        child: ColoredBox(
          color: Colors.white,
          child: Row(
            children: [
              Visibility(
                visible: !ResponsiveWidget.isSmallScreen(context),
                child: Text(
                  GoRouter.of(context).location.substring(1).capitalize(),
                  style: const TextStyle(color: Colors.black),
                )
              ),
              const Spacer(),
              IconButton(
                icon: const Icon(
                  Icons.logout,
                  color: Colors.black,
                ),
                onPressed: () {
                  context.read<LoginCubit>().logOut();
                  Future.microtask(() => context.go(AppRoutes.login));
                }
              ),
              const SizedBox(width: 24),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    sl<GlobalAuthState>().token!=null ? sl<GlobalAuthState>().token!.name : '',
                    style: const TextStyle(color: Colors.black),
                  ),
                  const SizedBox(
                    width: 16,
                  ),
                  InkWell(
                    radius: 30,
                    onTap: (){
                      context.go(AppRoutes.user);
                      context.read<NavigationCubit>().changeSelection(NavigationSelection.user);
                    },
                    child: DecoratedBox(
                      decoration:
                          BoxDecoration(borderRadius: BorderRadius.circular(30)),
                      child: Padding(
                        padding: const EdgeInsets.all(1),
                        child: DecoratedBox(
                          decoration: BoxDecoration(
                              color: Colors.black,
                              borderRadius: BorderRadius.circular(30)),
                          child: const Padding(
                            padding: EdgeInsets.all(1),
                            child: CircleAvatar(
                              child: Icon(Icons.person_outline),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 40,
                  )
                ],
              )
            ],
          ),
        ),
      ),
      elevation: 0,
      backgroundColor: Colors.transparent,
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(60);

}

extension StringExtension on String {
  String capitalize() {
    return "${this[0].toUpperCase()}${substring(1).toLowerCase()}";
  }
}
