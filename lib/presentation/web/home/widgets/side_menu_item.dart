import 'package:bloc_example/presentation/web/home/widgets/horizontal_menu_item.dart';
import 'package:bloc_example/presentation/web/home/widgets/vertical_menu_item.dart';
import 'package:bloc_example/presentation/web/home/widgets/web_responsive_widget.dart';
import 'package:flutter/material.dart';


class SideMenuItem extends StatelessWidget {
  final String itemName;
  final VoidCallback onTap;
  final Icon icon;
  final bool isHover;

  const SideMenuItem({ Key? key,required this.itemName,required this.onTap, required this.icon, required this.isHover }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if(ResponsiveWidget.isCustomSize(context)){
      return VerticalMenuItem(itemName: itemName, onTap: onTap,icon: icon, isHover: isHover);
    }else{
      return HorizontalMenuItem(itemName: itemName, onTap: onTap, icon: icon, isHover: isHover);
    }
  }
}