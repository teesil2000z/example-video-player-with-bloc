import 'package:bloc_example/app_routes.dart';
import 'package:bloc_example/di/home/navigation_cubit/navigation_cubit.dart';
import 'package:bloc_example/presentation/web/home/widgets/side_menu_item.dart';
import 'package:bloc_example/presentation/web/home/widgets/web_responsive_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

class SideMenu extends StatelessWidget {
  const SideMenu({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;

    return ListView(
      children: [
        if(ResponsiveWidget.isSmallScreen(context))
          Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              const SizedBox(
                height: 40,
              ),
              Row(
                children: [
                  SizedBox(width: width / 48),
                  const Flexible(
                    child: Text(
                      "Dash",
                    ),
                  ),
                  SizedBox(width: width / 48),
                ],
              ),
              const SizedBox(
                height: 30,
              ),
            ],
          ),
        Divider(color: Colors.grey.shade100),
        BlocBuilder<NavigationCubit, NavigationSelection>(
          builder: (context, state) {
            return Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SideMenuItem(
                  isHover: state == NavigationSelection.flow,
                  icon: const Icon(Icons.home),
                  itemName: 'Flow',
                  onTap: (){
                    context.read<NavigationCubit>().changeSelection(NavigationSelection.flow);
                    GoRouter.of(context).go(AppRoutes.flow);
                  },
                ),
                SideMenuItem(
                  isHover: state == NavigationSelection.user,
                  icon: const Icon(Icons.person),
                  itemName: 'User',
                  onTap: (){
                    context.read<NavigationCubit>().changeSelection(NavigationSelection.user);
                    GoRouter.of(context).go(AppRoutes.user);
                  },
                )
              ]
            );
          }
        )
      ],
    );
  }
}