import 'package:flutter/material.dart';

class HorizontalMenuItem extends StatelessWidget {
    final String itemName;
  final VoidCallback onTap;
  final Icon icon;
  final bool isHover;
  const HorizontalMenuItem({ Key? key, required this.itemName, required this.onTap, required this.isHover, required this.icon }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: ColoredBox(
        color: isHover ? Colors.purple.withOpacity(.1) : Colors.transparent,
        child: Row(
          children: [
            Visibility(
              visible: isHover,
              maintainSize: true,
              maintainAnimation: true,
              maintainState: true,
              child: const SizedBox(
                width: 6,
                height: 40,
                child: ColoredBox(color: Colors.purple,),
              ),
            ),
            SizedBox(width: MediaQuery.of(context).size.width / 88),
            Padding(
              padding: const EdgeInsets.all(16),
              child: icon,
            ),
            Flexible(child: Text(itemName))
          ],
        ),
      )
    );
  }
}