import 'package:bloc_example/di/home/user_bloc/user_bloc.dart';
import 'package:bloc_example/di/home/user_bloc/user_event.dart';
import 'package:bloc_example/presentation/web/home/widgets/side_menu.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';

class LargeScreen extends StatelessWidget {
  final Widget child;
  const LargeScreen({ Key? key, required this.child }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    context.read<UserBloc>().add(LoadUserEvent());
    return Row(crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Expanded(child: SideMenu()),
        Expanded(
          flex: 5,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: RepaintBoundary(child: child),
          )
        )
      ],
    );
  }
}
