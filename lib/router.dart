import 'package:bloc_example/app_routes.dart';
import 'package:bloc_example/model/global_auth_state.dart';
import 'package:bloc_example/presentation/mobile/auth/login_page.dart';
import 'package:bloc_example/presentation/mobile/create_video/page/create_video_page.dart';
import 'package:bloc_example/presentation/mobile/home/page/home_page.dart';
import 'package:bloc_example/presentation/mobile/home/widget/user_widget.dart';
import 'package:bloc_example/presentation/mobile/auth/sign_up_page.dart';
import 'package:bloc_example/presentation/mobile/home/widget/flow_widget.dart';
import 'package:bloc_example/presentation/mobile/video/page/video_page.dart';
import 'package:bloc_example/presentation/web/auth/web_login_page.dart';
import 'package:bloc_example/presentation/web/auth/web_sign_up_page.dart';
import 'package:bloc_example/presentation/web/create_video/page/web_create_video_page.dart';
import 'package:bloc_example/presentation/web/home/page/web_home_page.dart';
import 'package:bloc_example/presentation/web/home/widgets/web_flow_widget.dart';
import 'package:bloc_example/presentation/web/home/widgets/web_user_widget.dart';
import 'package:bloc_example/presentation/web/not_found/not_found_page.dart';
import 'package:bloc_example/presentation/web/video/page/web_video_page.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:go_router/go_router.dart';

final GlobalKey<NavigatorState> _rootNavigatorKey = GlobalKey<NavigatorState>();
final GlobalKey<NavigatorState> _shellNavigatorKey = GlobalKey<NavigatorState>();

final goRouter = GoRouter(
  initialLocation: AppRoutes.login,
  navigatorKey: _rootNavigatorKey,
  redirect: (context, state) async{
    if(state.location != AppRoutes.login && state.location != AppRoutes.signUp){
      if(GetIt.I<GlobalAuthState>().token==null || GetIt.I<GlobalAuthState>().token!.token==''){
        return AppRoutes.login;
      }
    }
    if(state.location == AppRoutes.login || state.location == AppRoutes.signUp){
      if(GetIt.I<GlobalAuthState>().token!=null && GetIt.I<GlobalAuthState>().token!.token!=''){
        return AppRoutes.flow;
      }
    }
    return null;
  },
  errorPageBuilder: (context, state) => MaterialPage(
        key: state.pageKey,
        child: const PageNotFound()
      ),
  routes: [
    GoRoute(
      path: AppRoutes.login,
      builder: (context, state){
        return kIsWeb ? const WebLoginPage() : const LoginPage();
      }
    ),
    GoRoute(
      path:  AppRoutes.signUp,
      builder: (context, state){
        return kIsWeb ? const WebSignUpPage() : const SignUpPage();
      }
    ),
    ShellRoute(
      navigatorKey: _shellNavigatorKey,
      builder: (context, state, child) {
        return kIsWeb ? WebHomeWidget(child: child) : HomePage(child: child);
      },
      routes: [
        GoRoute(
          path: AppRoutes.flow,
          pageBuilder: (context, state) => const NoTransitionPage(
            child: kIsWeb ? WebFlowWidget() : FlowWidget(),
          ),
          routes: [
            GoRoute(
              name: AppRoutes.flowVideo,
              path: 'video/:id',
              builder: (context, state) => kIsWeb 
                ? WebVideoPage(id: int.tryParse(state.pathParameters['id']!) ?? 0)
                : VideoPage(id: int.tryParse(state.pathParameters['id']!) ?? 0),
            ),
          ],
        ),
        GoRoute(
          path: AppRoutes.user,
          pageBuilder: (context, state) => const NoTransitionPage(
            child:  kIsWeb ? WebUserWidget() : UserWidget(),
          ),
          routes: [
            GoRoute(
              name: AppRoutes.userVideo,
              path: 'video/:id',
              builder: (context, state) => kIsWeb 
                ? WebVideoPage(id: int.tryParse(state.pathParameters['id']!) ?? 0) 
                : VideoPage(id: int.tryParse(state.pathParameters['id']!) ?? 0),
            ),
            GoRoute(
              name: AppRoutes.createVideo,
              path: 'create_video',
              builder: (context, state) => kIsWeb ? WebCreateVideoPage() : CreateVideoPage(),
            ),
          ],
        ),
      ],
    ),
  ],
);

